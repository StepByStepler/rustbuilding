package me.stepbystep.building;

import com.google.common.collect.ImmutableMap;
import lombok.RequiredArgsConstructor;
import lombok.val;
import me.stepbystep.building.obj.BlockLocation;
import me.stepbystep.building.obj.PatternBlock;
import me.stepbystep.building.pattern.PatternType;
import me.stepbystep.building.util.InvisibleEnchantment;
import me.stepbystep.building.util.PluginUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.server.PluginDisableEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Map;

@RequiredArgsConstructor
public class RustBuildingListener implements Listener {
    private static final String MENU_TITLE = "Выбор постройки";
    private final RustBuilding plugin;
    private final Map<Integer, PatternType> menuSlots = ImmutableMap.<Integer, PatternType>builder()
            .put(3, PatternType.BASEMENT)
            .put(5, PatternType.WALL)
            .put(11, PatternType.DOOR)
            .put(13, PatternType.FRAME)
            .put(15, PatternType.ROOF)
            .put(20, PatternType.STAIRS)
            .put(22, PatternType.RAMP)
            .put(24, PatternType.WINDOW)
            .build();

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        if (e.getHand() != EquipmentSlot.HAND) return;
        if (!e.hasItem()) return;
        val item = CraftItemStack.asNMSCopy(e.getItem());
        if (item.getTag() == null) return;

        if (item.getTag().getBoolean(PluginUtils.BUILD_ITEM_TAG)) {
//            onBuildingPlanInteract(e); // TODO: remove
        } else if (item.getTag().getBoolean(PluginUtils.BREAK_ITEM_TAG)) {
//            onHammerInteract(e);
        }
    }

    private void onBuildingPlanInteract(PlayerInteractEvent e) {
        val selectedPattern = PluginUtils.getSelectedPattern(CraftItemStack.asNMSCopy(e.getItem()));
        if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            openMenu(e.getPlayer(), selectedPattern);
        } else {
            performBuilding(e.getPlayer(), selectedPattern);
        }
    }

    // TODO: remove this
    @EventHandler
    public void onBlockBreakByPaper(BlockBreakEvent e) {
        if (e.getPlayer().getInventory().getItemInMainHand().getType() == Material.PAPER) {
            e.setCancelled(true);
        }
    }

//    @EventHandler
    public void onPluginDisable(PluginDisableEvent e) {
        if (e.getPlugin() == plugin) {
            plugin.getBlockStorage().save();
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent e) {
        plugin.getBlockStorage().removePatternBlocks(BlockLocation.fromBlock(e.getBlock()));
    }

//    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        val topInventory = e.getView().getTopInventory();
        if (topInventory.getName().equals(MENU_TITLE)) {
            e.setCancelled(true);
        }
        if (e.getRawSlot() < 0 || e.getRawSlot() >= topInventory.getSize()) return;

        val newPatternType = menuSlots.get(e.getRawSlot());
        if (newPatternType == null) return;

        val player = (CraftPlayer) e.getWhoClicked();
        PluginUtils.setSelectedPattern(player.getHandle().getItemInMainHand(), newPatternType);
        openMenu(player, newPatternType);
    }

    private void openMenu(Player player, PatternType selectedPattern) {
        val inventory = Bukkit.createInventory(null, 27, MENU_TITLE);
        val glass = new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.BLACK.getWoolData());
        for (int i = 0; i < inventory.getSize(); i++) {
            inventory.setItem(i, glass);
        }

        for (val patternEntry : menuSlots.entrySet()) {
            val currentPattern = patternEntry.getValue();
            val patternItem = new ItemStack(Material.WOOD);
            val meta = patternItem.getItemMeta();
            meta.setDisplayName(ChatColor.GREEN + currentPattern.getDisplayName());
            if (currentPattern == selectedPattern) {
                meta.addEnchant(InvisibleEnchantment.INSTANCE, 1, true);
            }
            patternItem.setItemMeta(meta);
            inventory.setItem(patternEntry.getKey(), patternItem);
        }

        player.openInventory(inventory);
    }

    private void performBuilding(Player player, PatternType patternType) {
        if (patternType == null) {
            player.sendMessage(ChatColor.RED + "Выберите тип постройки");
            return;
        }

        plugin.getApi().placeElement(player, patternType);
    }

    private BlockType chooseBlockType(PatternBlock oldBlock, BlockType newType) {
        return newType != null ? newType : oldBlock.getBlockType();
    }

    public void onHammerInteract(PlayerInteractEvent e) {
        if (e.getAction() != Action.LEFT_CLICK_BLOCK) return;
        val player = e.getPlayer();
        val blockLocation = BlockLocation.fromBlock(e.getClickedBlock());
        val allPatternBlocks = plugin.getBlockStorage().getAllPatternBlocks(blockLocation);

        if (allPatternBlocks.isEmpty()) return;
        if (allPatternBlocks.size() > 1) {
            player.sendMessage(ChatColor.RED + "Укажите на блок, принадлежащий одной конструкции");
            return;
        }

        val patternBlock = allPatternBlocks.getAny();
        val buildingElement = patternBlock.getBuildingElement();
        buildingElement.getBuilding().removeElement(buildingElement, new ArrayList<>(), true, true);
    }

    @EventHandler
    public void onItemDrop(PlayerDropItemEvent e) {
        val nmsPlayer = ((CraftPlayer) e.getPlayer()).getHandle();
        val inventory = nmsPlayer.inventory;
        val slot = inventory.itemInHandIndex;
        Bukkit.getScheduler().runTask(plugin, () -> {
            if (inventory.getItem(slot).isEmpty()) {
                inventory.setItem(slot, net.minecraft.server.v1_12_R1.ItemStack.a);
            }
        });
    }
}
