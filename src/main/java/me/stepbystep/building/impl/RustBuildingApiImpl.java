package me.stepbystep.building.impl;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.val;
import lombok.var;
import me.stepbystep.building.BlockType;
import me.stepbystep.building.RotationQuarter;
import me.stepbystep.building.RustBuilding;
import me.stepbystep.building.api.BuildingFilter;
import me.stepbystep.building.api.RustBuildingApi;
import me.stepbystep.building.obj.*;
import me.stepbystep.building.pattern.PatternType;
import me.stepbystep.building.util.PluginUtils;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.util.BlockIterator;
import org.eclipse.collections.api.list.MutableList;
import org.eclipse.collections.impl.list.mutable.FastList;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

@RequiredArgsConstructor
public class RustBuildingApiImpl implements RustBuildingApi {
    private final RustBuilding plugin;
    private final MutableList<BuildingFilter> filters = new FastList<>();
    private final MutableList<BuildingFilter> dynamicFilters = new FastList<>();
    private final LoadingCache<BuildingContext, ElementPlacementScope> scopeCache = CacheBuilder.newBuilder()
            .expireAfterAccess(2, TimeUnit.SECONDS)
            .build(new CacheLoader<BuildingContext, ElementPlacementScope>() {
                @Override
                public ElementPlacementScope load(@NonNull BuildingContext context) {
                    val scope = context.getPatternType().getLocations(context.getCenter(), context.getQuarter());
                    scope.setBuilding(findBuilding(scope));
                    if (!filtersSatisfied(filters, context, scope)) {
                        scope.setCanBePlaced(false);
                    }
                    return scope;
                }
            });

    {
        enableDefaultFilters();
    }

    private Building findBuilding(ElementPlacementScope scope) {
        Building building = null;
        for (val location : scope.getBlocks().keySet()) {
            val nearElements = location.findNearBuildingElements();
            for (val nearElement : nearElements) {
                val nearBuilding = nearElement.getBuilding();
                if (building == null) {
                    building = nearBuilding;
                } else if (building != nearBuilding) {
                    scope.setCanBePlaced(false);
                    return null;
                }
            }
        }

        return building;
    }

    @Override
    public Optional<BuildingElement> placeElement(@NonNull Player player, @NonNull PatternType patternType) {
        val scope = findElementPlace(player, patternType);

        if (!scope.canBePlaced()) return Optional.empty();
        // center should be never truncated
        if (!scope.getLocations().contains(scope.getCenter())) return Optional.empty();

        val blockStorage = plugin.getBlockStorage();
        val buildingElement = BuildingElement.create(scope.getCenter(), patternType, 0, scope.getRotation());
        var building = scope.getBuilding();

        var index = -1;
        for (val entry : scope.getBlocks().entrySet()) {
            index++;

            val location = entry.getKey();
            val oldPatternBlock = blockStorage.getAnyPatternBlock(location);
            if (oldPatternBlock == null && entry.getValue() == null) continue;
            val blockType = entry.getValue() != null ? entry.getValue() : oldPatternBlock.getBlockType();
            val patternBlock = new PatternBlock(buildingElement, blockType, index, location);
            buildingElement.getBlocks().add(patternBlock);

            if (blockType != BlockType.AIR &&
                    (oldPatternBlock == null || blockType.getPriority() > oldPatternBlock.getBlockType().getPriority())
            ) {
                blockType.applyTo(location.getBlock(), 0, buildingElement.getRotation());
            }
        }
        buildingElement.updateBoundingBox(); // must be before building::recomputeStability call

        if (building == null) {
            building = new Building();
            building.recomputeStability(new ArrayList<>()); // fill graph field
            blockStorage.addBuilding(building);
        }

        buildingElement.setBuilding(building);
        blockStorage.addBuildingElement(buildingElement);

        if (building.canElementBeAdded(buildingElement)) {
            building.getElements().add(buildingElement);
            building.updateBoundingBox();
            scopeCache.invalidateAll();
        } else {
            blockStorage.removeBuildingElement(buildingElement);
            building.removeElement(buildingElement, new ArrayList<>(), true, false);
        }

        return Optional.of(buildingElement);
    }

    @Override
    public boolean isDoorwayBlock(@NonNull Location location) {
        val blockLocation = BlockLocation.fromLocation(location);
        val patternBlock = plugin.getBlockStorage().getAnyPatternBlock(blockLocation);
        if (patternBlock == null) return false;
        return patternBlock.getPatternType() == PatternType.DOOR && patternBlock.getLocation().equals(blockLocation);
    }

    @Override
    public void upgradeElement(@NonNull Player player, int newLevel) {
        if (newLevel >= PluginUtils.LEVELS_COUNT) {
            throw new IllegalArgumentException("New level must be in range [1; " + PluginUtils.MAX_LEVEL + ")");
        }

        val targetBlock = player.getTargetBlock(null, 5);
        if (targetBlock.getType() == Material.AIR) return;

        val blockStorage = RustBuilding.INSTANCE.getBlockStorage();
        val patternBlocks = blockStorage.getAllPatternBlocks(BlockLocation.fromBlock(targetBlock));
        if (patternBlocks.size() != 1) return;

        val patternBlock = patternBlocks.getAny();
        upgradeElement(patternBlock.getBuildingElement(), newLevel);
    }

    @Override
    public void upgradeElement(@NonNull BuildingElement element, int newLevel) {
        val blockStorage = RustBuilding.INSTANCE.getBlockStorage();
        for (val blockOfElement : element.getBlocks()) {
            val blockType = blockOfElement.getBlockType();
            val allBlocksAtPoints = blockStorage.getAllPatternBlocks(blockOfElement.getLocation());
            if (allBlocksAtPoints.anySatisfy(block -> block.getBlockType().getPriority() > blockType.getPriority()))
                continue;

            val currentBlockLevel = allBlocksAtPoints.stream()
                    .filter(block -> block.getBlockType().getPriority() == blockType.getPriority())
                    .max(Comparator.comparing(PatternBlock::getLevel))
                    .orElseThrow(IllegalStateException::new)
                    .getLevel();
            if (currentBlockLevel >= newLevel) continue;

            val materialData = blockType.getMaterialData(newLevel);
            if (materialData.getMaterial() == Material.AIR) continue;

            val block = blockOfElement.getLocation().getBlock();
            blockType.applyTo(block, newLevel, element.getRotation());
        }

        element.setLevel(newLevel);
    }

    @Override
    public void breakElement(@NonNull Player player, boolean fireEvents) {
        val targetBlock = player.getTargetBlock(null, 5);
        val blockLocation = BlockLocation.fromBlock(targetBlock);
        val allPatternBlocks = plugin.getBlockStorage().getAllPatternBlocks(blockLocation);

        if (allPatternBlocks.size() != 1) return;

        val patternBlock = allPatternBlocks.getAny();
        breakElement(patternBlock.getBuildingElement(), fireEvents);
    }

    @Override
    public List<BuildingElement> breakElement(@NonNull BuildingElement element, boolean fireEvents) {
        val result = new ArrayList<BuildingElement>();
        element.getBuilding().removeElement(element, result, fireEvents, true);
        scopeCache.invalidateAll();
        return result;
    }

    @Override
    public Optional<PatternBlock> getSingleBlockAt(@NonNull Location location) {
        val blockLocation = BlockLocation.fromLocation(location);
        val allBlocks = plugin.getBlockStorage().getAllPatternBlocks(blockLocation);
        if (allBlocks.size() == 1)
            return Optional.of(allBlocks.getAny());
        else
            return Optional.empty();
    }

    @Override
    public ElementPlacementScope findElementPlace(@NonNull Player player, @NonNull PatternType patternType) {
        val nmsPlayer = ((CraftPlayer) player).getHandle();
        if (patternType == PatternType.BASEMENT) {
            val centerBlock = PluginUtils.findCenterLocation(player, patternType);
            val centerLocation = BlockLocation.fromLocation(centerBlock);
            val quarter = RotationQuarter.forYaw(nmsPlayer.yaw);
            val context = new BuildingContext(player, patternType, centerLocation, quarter);
            return checkDynamicFilters(context, scopeCache.getUnchecked(context));
        } else {
            val iterator = new BlockIterator(player, 5);
            while (iterator.hasNext()) {
                val centerBlock = iterator.next();
                val centerLocation = BlockLocation.fromBlock(centerBlock);
                val quarter = RotationQuarter.forYaw(nmsPlayer.yaw);
                val context = new BuildingContext(player, patternType, centerLocation, quarter);
                val scope = checkDynamicFilters(context, scopeCache.getUnchecked(context));
                if (scope.canBePlaced() || !iterator.hasNext()) {
                    return scope;
                }
            }
            throw new IllegalStateException("Cannot reach there");
        }
    }

    private ElementPlacementScope checkDynamicFilters(BuildingContext ctx, ElementPlacementScope scope) {
        val filtersSatisfied = filtersSatisfied(dynamicFilters, ctx, scope);
        scope.setDynamicFiltersSatisfied(filtersSatisfied);
        return scope;
    }

    @Override
    public void addFilter(@NonNull BuildingFilter filter) {
        filters.add(filter);
    }

    @Override
    public void addDynamicFilter(@NonNull BuildingFilter filter) {
        dynamicFilters.add(filter);
    }

    @Override
    public Optional<BuildingElement> findBuilding(@NonNull Location location) {
        val blockStorage = plugin.getBlockStorage();
        val blockLocation = BlockLocation.fromLocation(location);
        for (int dy = -1; dy >= -5; dy--) {
            val bottomLoc = blockLocation.getRelative(0, dy, 0);
            val patternBlock = blockStorage.getAnyPatternBlock(bottomLoc);
            if (patternBlock != null) {
                return Optional.of(patternBlock.getBuildingElement());
            }
        }

        return Optional.empty();
    }

    private boolean filtersSatisfied(
            MutableList<BuildingFilter> filters,
            BuildingContext ctx,
            ElementPlacementScope scope
    ) {
        return filters.allSatisfy(predicate -> predicate.test(ctx, scope));
    }

    private void enableDefaultFilters() {
        addFilter((ctx, scope) -> {
            val building = scope.getBuilding();
            val newElementBox = PluginUtils.computeBoundingBox(scope.getLocations(), Function.identity());

            return RustBuilding.INSTANCE.getBlockStorage().getBuildings().allSatisfy(otherBuilding -> {
                if (building == otherBuilding) return true;
                if (!otherBuilding.getBoundingBox().overlaps(newElementBox)) return true;

                return otherBuilding.getElements().stream()
                        .noneMatch(otherElement -> otherElement.getBoundingBox().overlaps(newElementBox));
            });
        });
    }
}
