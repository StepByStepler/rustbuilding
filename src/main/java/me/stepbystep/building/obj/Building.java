package me.stepbystep.building.obj;

import lombok.Data;
import lombok.val;
import lombok.var;
import me.stepbystep.building.BlockType;
import me.stepbystep.building.RustBuilding;
import me.stepbystep.building.api.event.BuildingElementBreakEvent;
import me.stepbystep.building.pattern.PatternType;
import me.stepbystep.building.util.BoundingBox;
import org.bukkit.Material;
import org.jgrapht.Graph;
import org.jgrapht.alg.interfaces.ShortestPathAlgorithm;
import org.jgrapht.alg.shortestpath.BFSShortestPath;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleWeightedGraph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Data
public class Building {
    private final List<BuildingElement> elements = new ArrayList<>();
    private BoundingBox boundingBox = null;
    private Graph<BuildingElement, DefaultWeightedEdge> elementsGraph = null;

    public void recomputeStability(List<BuildingElement> removedElements) {
        val graph = new SimpleWeightedGraph<BuildingElement, DefaultWeightedEdge>(null, DefaultWeightedEdge::new);
        elementsGraph = graph;
        elements.forEach(graph::addVertex);
        for (val element : elements) {
            if (element.getPatternType() == PatternType.RAMP) {
                addEdges(graph, element.getCenter(), element);
            } else {
                element.getBlocks().forEach(block -> addEdges(graph, block.getLocation(), element));
            }
        }

        val stabilities = new HashMap<BuildingElement, Double>();
        for (val element : elements) {
            stabilities.put(element, 1.0);
        }

        val shortestPathAlgorithm = new BFSShortestPath<>(graph);
        for (val element : elements) {
            if (element.getPatternType() != PatternType.BASEMENT) continue;
            stabilities.put(element, 0.0);

            ShortestPathAlgorithm.SingleSourcePaths<BuildingElement, DefaultWeightedEdge> paths = null;
            for (val otherElement : elements) {
                val currentStability = stabilities.get(otherElement);
                if (currentStability <= 0.9) continue;
                if (otherElement.getPatternType() == PatternType.BASEMENT) continue;
                if (paths == null) {
                     paths = shortestPathAlgorithm.getPaths(element);
                }
                val shortestPath = paths.getPath(otherElement);
                if (shortestPath == null) continue;
                if (otherElement.getPatternType() == PatternType.RAMP && shortestPath.getLength() != 1) continue;
                val amplifier = Math.pow(shortestPath.getWeight(), 1.05) / 12;
                if (amplifier >= 1) continue;
                stabilities.put(otherElement, currentStability * amplifier);
            }
        }

        for (val entry : stabilities.entrySet()) {
            var removedAnyElement = false;
            if (entry.getValue() > 0.9) {
                removeElement(entry.getKey(), removedElements, true, false);
                removedAnyElement = true;
            }
            if (removedAnyElement) {
                recomputeStability(removedElements);
            }
        }

        updateBoundingBox();
    }

    public boolean canElementBeAdded(BuildingElement element) {
        val graph = elementsGraph;
        if (graph == null) throw new IllegalStateException("elementsGraph not initialized at " + this);

        graph.addVertex(element);
        if (element.getPatternType() == PatternType.BASEMENT) return true;

        if (element.getPatternType() == PatternType.RAMP) {
            addEdges(graph, element.getCenter(), element);
        } else {
            element.getBlocks().forEach(block -> addEdges(graph, block.getLocation(), element));
        }

        var stability = 1.0;
        val dijkstraShortestPath = new DijkstraShortestPath<>(graph);
        for (val basementElement : elements) {
            if (basementElement.getPatternType() != PatternType.BASEMENT) continue;

            val shortestPath = dijkstraShortestPath.getPath(basementElement, element);
            if (shortestPath == null) continue;
            if (element.getPatternType() == PatternType.RAMP && shortestPath.getLength() != 1) continue;
            val amplifier = Math.pow(shortestPath.getWeight(), 1.05) / 12;
            if (amplifier >= 1) continue;
            stability *= amplifier;
        }

        val canBeAdded = stability <= 0.9;
        if (!canBeAdded) {
            graph.removeVertex(element);
        }

        return canBeAdded;
    }

    private void addEdges(Graph<BuildingElement, DefaultWeightedEdge> graph, BlockLocation location, BuildingElement element) {
        addEdge(graph, location, element, 1, 0, 0, 9);
        addEdge(graph, location, element, 0, 1, 0, 1);
        addEdge(graph, location, element, 0, 0, 1, 9);
        addEdge(graph, location, element, -1, 0, 0, 9);
        addEdge(graph, location, element, 0, -1, 0, 1);
        addEdge(graph, location, element, 0, 0, -1, 9);
    }

    private void addEdge(
            Graph<BuildingElement, DefaultWeightedEdge> graph,
            BlockLocation location,
            BuildingElement element,
            int dx, int dy, int dz,
            int weight
    ) {
        val nearLoc = location.getRelative(dx, dy, dz);
        val otherBlocks = RustBuilding.INSTANCE.getBlockStorage().getAllPatternBlocks(nearLoc);
        if (otherBlocks.anySatisfy(otherBlock -> otherBlock.getBuildingElement().equals(element))) return;

        for (val block : otherBlocks) {
            val otherElement = block.getBuildingElement();
            if (otherElement.getPatternType() == PatternType.RAMP || otherElement == element) continue;

            val edge = graph.getEdge(element, otherElement);
            if (edge != null) {
                val existingWeight = graph.getEdgeWeight(edge);
                if (existingWeight > weight) {
                    graph.setEdgeWeight(edge, weight);
                }
            } else {
                val newEdge = graph.addEdge(element, otherElement);
                graph.setEdgeWeight(newEdge, weight);
            }
        }
    }

    public void removeElement(
            BuildingElement element, List<BuildingElement> removedElements,
            boolean fireEvent, boolean performStabilityRecompute
    ) {
        elements.remove(element);
        removedElements.add(element);

        if (fireEvent) {
            new BuildingElementBreakEvent(element).callEvent();
        }

        val blockStorage = RustBuilding.INSTANCE.getBlockStorage();
        blockStorage.removeBuildingElement(element);
        for (val block : element.getBlocks()) {
            val otherPatternBlock = blockStorage.getAllPatternBlocks(block.getLocation())
                    .maxByOptional(patternBlock -> patternBlock.getBlockType().getPriority())
                    .orElse(null);
            val blockLocation = block.getLocation().asBukkitLocation();
            val bukkitBlock = blockLocation.getBlock();
            if (otherPatternBlock == null) {
                if (block.getBlockType() != BlockType.AIR) {
                    bukkitBlock.setType(Material.AIR);
                }
            } else {
                val newLevel = otherPatternBlock.getLevel();
                val rotation = otherPatternBlock.getBuildingElement().getRotation();
                otherPatternBlock.getBlockType().applyTo(bukkitBlock, newLevel, rotation);
            }
        }

        if (elements.isEmpty()) {
            blockStorage.removeBuilding(this);
        } else if (performStabilityRecompute) {
            recomputeStability(removedElements);
        }
    }

    public void updateBoundingBox() {
        boundingBox = computeBoundingBox();
    }

    private BoundingBox computeBoundingBox() {
        if (elements.isEmpty()) return BoundingBox.EMPTY;

        BoundingBox result = null;
        for (val element : elements) {
            val elementBox = element.getBoundingBox();
            if (result == null) {
                result = elementBox.clone();
            } else {
                result.unionWithoutCheck(elementBox);
            }
        }

        return result;
    }

    @Override
    public boolean equals(Object obj) {
        return this == obj;
    }
}
