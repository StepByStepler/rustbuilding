package me.stepbystep.building.obj;

import lombok.Data;
import lombok.val;
import me.stepbystep.building.RustBuilding;
import me.stepbystep.building.util.PluginUtils;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.util.Vector;

import java.util.HashSet;
import java.util.Set;

@Data
public class BlockLocation {
    private final World world;
    private final int x;
    private final int y;
    private final int z;

    public static BlockLocation fromBlock(Block block) {
        return new BlockLocation(block.getWorld(), block.getX(), block.getY(), block.getZ());
    }

    public static BlockLocation fromLocation(Location location) {
        return new BlockLocation(location.getWorld(), location.getBlockX(), location.getBlockY(), location.getBlockZ());
    }

    public Block getBlock() {
        return world.getBlockAt(x, y, z);
    }

    public Block getBlock(int dx, int dy, int dz) {
        return world.getBlockAt(x + dx, y + dy, z + dz);
    }

    public Location asBukkitLocation() {
        return new Location(world, x, y, z);
    }

    public BlockLocation getRelative(int dx, int dy, int dz) {
        return new BlockLocation(world, x + dx, y + dy, z + dz);
    }

    public Set<BuildingElement> findNearBuildingElements() {
        val result = new HashSet<BuildingElement>();
        val blockStorage = RustBuilding.INSTANCE.getBlockStorage();
        for (val point : PluginUtils.blockNeighbours) {
            val nearLoc = getRelative(point.getFirst(), point.getSecond(), point.getThird());
            for (val patternBlock : blockStorage.getAllPatternBlocks(nearLoc)) {
                result.add(patternBlock.getBuildingElement());
            }
        }

        return result;
    }

    public Vector subtract(BlockLocation other) {
        return new Vector(x - other.x, y - other.y, z - other.z);
    }
}
