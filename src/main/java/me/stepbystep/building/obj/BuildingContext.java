package me.stepbystep.building.obj;

import lombok.Data;
import me.stepbystep.building.RotationQuarter;
import me.stepbystep.building.pattern.PatternType;
import org.bukkit.entity.Player;

@Data
public class BuildingContext {
    private final Player player;
    private final PatternType patternType;
    private final BlockLocation center;
    private final RotationQuarter quarter;
}
