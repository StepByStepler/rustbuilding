package me.stepbystep.building.obj;

import lombok.AllArgsConstructor;
import lombok.Data;
import me.stepbystep.building.BlockType;
import me.stepbystep.building.pattern.PatternType;

@Data
@AllArgsConstructor
public final class PatternBlock {
    private BuildingElement buildingElement; // can be changed only in BlockStorage::loadBlocks
    private final BlockType blockType;
    private final int blockIndex;
    private final BlockLocation location;

    public PatternType getPatternType() {
        return buildingElement.getPatternType();
    }

    public int getLevel() {
        return buildingElement.getLevel();
    }
}
