package me.stepbystep.building.obj;

import lombok.*;
import me.stepbystep.building.BlockType;
import me.stepbystep.building.RotationQuarter;
import me.stepbystep.building.pattern.PatternType;
import me.stepbystep.building.util.BoundingBox;
import me.stepbystep.building.util.PluginUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@EqualsAndHashCode(exclude = { "blocks", "building", "level", "boundingBox" })
@ToString(exclude = { "blocks", "building" })
public class BuildingElement {
    private final List<PatternBlock> blocks = new ArrayList<>();
    private Building building;
    private final BlockLocation center;
    private final PatternType patternType;
    private int level;
    private final RotationQuarter rotation;
    private BoundingBox boundingBox;

    public static BuildingElement create(
            BlockLocation center, PatternType patternType, int level, RotationQuarter rotation
    ) {
        return new BuildingElement(null, center, patternType, level, rotation, BoundingBox.EMPTY);
    }

    public List<PatternBlock> getInnerBlocks() {
        return blocks.stream()
                .filter(block -> {
                    val blockType = block.getBlockType();
                    return blockType == BlockType.INNER || blockType == BlockType.FAKE_BASEMENT_INNER;
                })
                .collect(Collectors.toList());
    }

    public boolean exists() {
        return building != null && building.getElements().contains(this);
    }

    public void updateBoundingBox() {
        boundingBox = PluginUtils.computeBoundingBox(blocks, PatternBlock::getLocation);
    }
}
