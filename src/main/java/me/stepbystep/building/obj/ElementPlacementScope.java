package me.stepbystep.building.obj;

import lombok.*;
import me.stepbystep.building.BlockType;
import me.stepbystep.building.RotationQuarter;
import me.stepbystep.building.pattern.PatternType;

import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
public final class ElementPlacementScope {
    private final Map<BlockLocation, BlockType> blocks;
    private final List<BlockLocation> locations;
    private final BlockLocation center;
    private final RotationQuarter rotation;
    private final PatternType patternType;
    @Getter(value = AccessLevel.NONE)
    private boolean canBePlaced;
    private Building building;
    private boolean dynamicFiltersSatisfied;

    public boolean canBePlaced() {
        return canBePlaced && dynamicFiltersSatisfied;
    }
}
