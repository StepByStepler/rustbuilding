package me.stepbystep.building.db;

import com.google.common.collect.HashMultimap;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.val;
import lombok.var;
import me.stepbystep.building.BlockType;
import me.stepbystep.building.RotationQuarter;
import me.stepbystep.building.RustBuilding;
import me.stepbystep.building.obj.BlockLocation;
import me.stepbystep.building.obj.Building;
import me.stepbystep.building.obj.BuildingElement;
import me.stepbystep.building.obj.PatternBlock;
import me.stepbystep.building.pattern.PatternType;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.plugin.Plugin;
import org.eclipse.collections.api.collection.MutableCollection;
import org.eclipse.collections.api.multimap.MutableMultimap;
import org.eclipse.collections.api.set.MutableSet;
import org.eclipse.collections.impl.multimap.set.UnifiedSetMultimap;
import org.eclipse.collections.impl.set.mutable.UnifiedSet;
import org.h2.Driver;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.UUID;
import java.util.logging.Level;

public class BlockStorage {
    @Getter
    private final MutableSet<Building> buildings;
    private final MutableMultimap<BlockLocation, PatternBlock> blocks = new UnifiedSetMultimap<>();
    private final HikariDataSource dataSource;

    public BlockStorage(RustBuilding plugin) {
        var millisNow = System.currentTimeMillis();
        dataSource = loadDataSource(plugin);
        System.out.println("Load data source took " + -(millisNow - (millisNow = System.currentTimeMillis())) + " ms");
        createTables();
        System.out.println("Create tables took " + -(millisNow - (millisNow = System.currentTimeMillis())) + " ms");
        buildings = loadBlocks();
        System.out.println("Load blocks took " + -(millisNow - (millisNow = System.currentTimeMillis())) + " ms");
        save(); // force JVM to load classes required for saving
        System.out.println("Initial save took " + -(millisNow - (millisNow = System.currentTimeMillis())) + " ms");
    }

    private HikariDataSource loadDataSource(Plugin plugin) {
        val configSection = plugin.getConfig().getConfigurationSection("database");
        val hikariConfig = new HikariConfig();

        hikariConfig.setJdbcUrl(configSection.getString("url"));
        hikariConfig.setUsername(configSection.getString("username"));
        hikariConfig.setPassword(configSection.getString("password"));
        hikariConfig.setDriverClassName(Driver.class.getName());
        hikariConfig.addDataSourceProperty("serverTimezone", "UTC");

        return new HikariDataSource(hikariConfig);
    }

    @SneakyThrows
    private void createTables() {
        try (val connection = dataSource.getConnection();
             val statement = connection.createStatement()) {
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS BuildingElements(" +
                    "buildingID VARCHAR(36) NOT NULL, " +
                    "elementID VARCHAR(36) NOT NULL, " +
                    "centerLocation TEXT NOT NULL, " +
                    "patternType VARCHAR(32) NOT NULL, " +
                    "level INTEGER NOT NULL, " +
                    "rotation INTEGER NOT NULL" +
                    ")");

            statement.executeUpdate("CREATE TABLE IF NOT EXISTS PatternBlocks(" +
                    "elementID VARCHAR(36) NOT NULL, " +
                    "blockType VARCHAR(32) NOT NULL, " +
                    "blockIndex INTEGER NOT NULL, " +
                    "location TEXT NOT NULL" +
                    ")");

            statement.executeUpdate("CREATE INDEX IF NOT EXISTS idIndex ON PatternBlocks(elementID)");
            createColumnIfNotExists(connection, "BuildingElements", "rotation", "INTEGER DEFAULT 0 NOT NULL", "0");
        }
    }

    @SneakyThrows
    private void createColumnIfNotExists(Connection connection, String table, String column, String columnDefinition, String defaultValue) {
        try (val statement = connection.createStatement()) {
            statement.executeUpdate("ALTER TABLE " + table + " ADD COLUMN IF NOT EXISTS " + column + " " + columnDefinition);
        }
    }

    @SneakyThrows
    private MutableSet<Building> loadBlocks() {
        val buildings = new HashMap<UUID, Building>();
        try (val connection = dataSource.getConnection()) {
            val blocksByID = HashMultimap.<UUID, PatternBlock>create();
            val blocksStatement = connection.prepareStatement("SELECT * FROM PatternBlocks");
            val blocksResult = blocksStatement.executeQuery();
            while (blocksResult.next()) {
                val blockType = BlockType.valueOf(blocksResult.getString(2));
                val blockIndex = blocksResult.getInt(3);
                val location = optimized(BlockLocation.class, parseLocation(blocksResult.getString(4)));
                val block = new PatternBlock(null, blockType, blockIndex, location);
                blocksByID.put(UUID.fromString(blocksResult.getString(1)), block);
            }

            val elementStatement = connection.prepareStatement("SELECT * FROM BuildingElements");

            val selectQuery = elementStatement.executeQuery();
            while (selectQuery.next()) {
                val buildingID = optimized(UUID.class, UUID.fromString(selectQuery.getString(1)));
                val elementID = optimized(UUID.class, UUID.fromString(selectQuery.getString(2)));
                val centerLoc = optimized(BlockLocation.class, parseLocation(selectQuery.getString(3)));
                val patternType = PatternType.valueOf(selectQuery.getString(4));
                val level = selectQuery.getInt(5);
                val rotation = RotationQuarter.values()[selectQuery.getInt(6)];
                val buildingElement = BuildingElement.create(centerLoc, patternType, level, rotation);

                val elementBlocks = blocksByID.get(elementID);
                for (val block : elementBlocks) {
                    block.setBuildingElement(buildingElement);
                    buildingElement.getBlocks().add(block);
                    blocks.put(block.getLocation(), block);
                }
                buildingElement.updateBoundingBox(); // must be before building::updateBoundingBox call

                val building = buildings.computeIfAbsent(buildingID, id -> new Building());
                buildingElement.setBuilding(building);
                building.getElements().add(buildingElement);
                building.updateBoundingBox();
            }
        }
        Bukkit.getScheduler().runTask(RustBuilding.INSTANCE, () -> {
            for (val building : buildings.values()) {
                try {
                    building.recomputeStability(new ArrayList<>());
                } catch (Throwable t) {
                    Bukkit.getLogger().log(Level.WARNING, "Exception occurred while preparing building " + building);
                    t.printStackTrace();
                }
            }
        });
        ObjectPoolOptimizer.clear();
        return new UnifiedSet<>(buildings.values());
    }

    private BlockLocation parseLocation(String text) {
        val args = text.split(";");
        if (Bukkit.getWorld(args[0]) == null) {
            System.out.println("World with name " + args[0] + " doesn't exist!");
        }
        return new BlockLocation(
                Bukkit.getWorld(args[0]),
                Integer.parseInt(args[1]),
                Integer.parseInt(args[2]),
                Integer.parseInt(args[3])
        );
    }

    @SneakyThrows
    public void save() {
        try (val connection = dataSource.getConnection();
             val statement = connection.createStatement()) {
            statement.executeUpdate("DELETE FROM BuildingElements");
            statement.executeUpdate("DELETE FROM PatternBlocks");
            val insertElementStatement = connection.prepareStatement("INSERT INTO BuildingElements VALUES (?, ?, ?, ?, ?, ?)");
            val insertBlockStatement = connection.prepareStatement("INSERT INTO PatternBlocks VALUES (?, ?, ?, ?)");

            val buildings = new HashSet<Building>();
            for (val blocks : blocks.toMap().values()) {
                for (val block : blocks) {
                    buildings.add(block.getBuildingElement().getBuilding());
                }
            }
            for (val building : buildings) {
                val buildingID = UUID.randomUUID().toString();
                for (val element : building.getElements()) {
                    val elementID = UUID.randomUUID().toString();
                    insertElementStatement.setString(1, buildingID);
                    insertElementStatement.setString(2, elementID);
                    insertElementStatement.setString(3, serializeLocation(element.getCenter()));
                    insertElementStatement.setString(4, element.getPatternType().getName());
                    insertElementStatement.setInt(5, element.getLevel());
                    insertElementStatement.setInt(6, element.getRotation().ordinal());
                    insertElementStatement.executeUpdate();

                    for (val block : element.getBlocks()) {
                        insertBlockStatement.setString(1, elementID);
                        insertBlockStatement.setString(2, block.getBlockType().name());
                        insertBlockStatement.setInt(3, block.getBlockIndex());
                        insertBlockStatement.setString(4, serializeLocation(block.getLocation()));
                        insertBlockStatement.executeUpdate();
                    }
                }
            }
        }
    }

    private String serializeLocation(BlockLocation blockLocation) {
        return blockLocation.getWorld().getName() + ';' +
                blockLocation.getX() + ';' +
                blockLocation.getY() + ';' +
                blockLocation.getZ();
    }

    public PatternBlock getAnyPatternBlock(Block block) {
        return blocks.get(BlockLocation.fromBlock(block)).getAny();
    }

    public PatternBlock getAnyPatternBlock(BlockLocation location) {
        return blocks.get(location).getAny();
    }

    public PatternBlock getPatternBlockWithType(BlockLocation location, PatternType type) {
        val allBlocks = getAllPatternBlocks(location);
        for (val block : allBlocks) {
            if (block.getPatternType() == type) {
                return block;
            }
        }

        return null;
    }

    public PatternBlock getPatternBlockOfType(BlockLocation location, PatternType type) {
        val allBlocks = getAllPatternBlocks(location);
        for (val block : allBlocks) {
            if (block.getPatternType() == type) {
                return block;
            }
        }

        return null;
    }

    public PatternBlock getPatternBlockOfDifferentType(BlockLocation location, PatternType type) {
        val allBlocks = getAllPatternBlocks(location);
        for (val block : allBlocks) {
            if (block.getPatternType() != type) {
                return block;
            }
        }

        return null;
    }

    public MutableCollection<PatternBlock> getAllPatternBlocks(BlockLocation location) {
        return blocks.get(location);
    }

    public boolean hasPatternBlock(BlockLocation location) {
        return blocks.containsKey(location);
    }

    public boolean hasPatternBlock(Location location) {
        return hasPatternBlock(BlockLocation.fromLocation(location));
    }

    public void removePatternBlocks(BlockLocation location) {
        blocks.removeAll(location);
    }

    public void addBuilding(Building building) {
        buildings.add(building);
    }

    public void removeBuilding(Building building) {
        buildings.remove(building);
    }

    public void addBuildingElement(BuildingElement element) {
        for (val block : element.getBlocks()) {
            blocks.put(block.getLocation(), block);
        }
    }

    public void removeBuildingElement(BuildingElement element) {
        for (val block : element.getBlocks()) {
            blocks.remove(block.getLocation(), block);
        }
    }

    private <T> T optimized(Class<T> clazz, T value) {
        return ObjectPoolOptimizer.forClass(clazz).get(value);
    }
}
