package me.stepbystep.building.db;

import java.util.HashMap;
import java.util.Map;

public class ObjectPoolOptimizer<T> {
    private static final Map<Class<?>, ObjectPoolOptimizer<?>> pools = new HashMap<>();
    private final Map<T, T> objects = new HashMap<>();

    @SuppressWarnings("unchecked")
    public static <T> ObjectPoolOptimizer<T> forClass(Class<T> clazz) {
        return (ObjectPoolOptimizer<T>) pools.computeIfAbsent(clazz, c -> new ObjectPoolOptimizer<>());
    }

    public static void clear() {
        pools.clear();
    }

    public T get(T value) {
        return objects.computeIfAbsent(value, key -> value);
    }
}
