package me.stepbystep.building;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import me.stepbystep.building.util.IntPair;
import org.bukkit.util.Vector;

@RequiredArgsConstructor
public enum RotationQuarter {
    FIRST(0) {
        @Override
        public double rotateX(Vector vector) {
            return vector.getX();
        }

        @Override
        public double rotateZ(Vector vector) {
            return vector.getZ();
        }
    },
    SECOND(-90) {
        @Override
        public double rotateX(Vector vector) {
            return vector.getZ();
        }

        @Override
        public double rotateZ(Vector vector) {
            return -vector.getX();
        }
    },
    THIRD(180) {
        @Override
        public double rotateX(Vector vector) {
            return -vector.getX();
        }

        @Override
        public double rotateZ(Vector vector) {
            return -vector.getZ();
        }
    },
    FOURTH(90) {
        @Override
        public double rotateX(Vector vector) {
            return -vector.getZ();
        }

        @Override
        public double rotateZ(Vector vector) {
            return vector.getX();
        }
    },
    ;

    @Getter private final double yaw;

    public abstract double rotateX(Vector vector);
    public abstract double rotateZ(Vector vector);

    public IntPair rotate2D(Vector vector) {
        return new IntPair((int) rotateX(vector), (int) rotateZ(vector));
    }

    public RotationQuarter opposite() {
        switch (this) {
            case FIRST: return THIRD;
            case SECOND: return FOURTH;
            case THIRD: return FIRST;
            case FOURTH: return SECOND;
        }

        throw new IllegalStateException("Switch is not exhaustive");
    }

    public static RotationQuarter forYaw(float yaw) {
        float normalizedYaw;
        if (yaw >= -180 && yaw <= 180)
            normalizedYaw = yaw;
        else if (yaw >= -540 && yaw <= -180)
            normalizedYaw = yaw + 360;
        else if (yaw >= 180 && yaw <= 540)
            normalizedYaw = yaw - 360;
        else {
            int periods = (int) yaw / 360;
            return forYaw(yaw - periods * 360);
        }

        if (normalizedYaw >= -45 && normalizedYaw <= 45)
            return FIRST;
        else if (normalizedYaw >= -135 && normalizedYaw <= -45)
            return SECOND;
        else if (normalizedYaw >= -180 && normalizedYaw <= -135 || normalizedYaw >= 135 && normalizedYaw <= 180)
            return THIRD;
        else if (normalizedYaw >= 45 && normalizedYaw <= 135)
            return FOURTH;
        else
            throw new IllegalStateException("Unknown normalizedYaw: " + normalizedYaw + " for yaw: " + yaw);
    }
}
