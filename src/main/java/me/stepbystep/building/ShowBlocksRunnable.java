package me.stepbystep.building;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.EnumWrappers;
import com.comphenix.protocol.wrappers.WrappedDataWatcher;
import com.comphenix.protocol.wrappers.WrappedWatchableObject;
import lombok.*;
import me.stepbystep.building.obj.ElementPlacementScope;
import me.stepbystep.building.pattern.PatternType;
import me.stepbystep.building.util.PluginUtils;
import net.minecraft.server.v1_12_R1.*;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;
import org.eclipse.collections.api.tuple.Pair;
import org.eclipse.collections.impl.tuple.Tuples;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class ShowBlocksRunnable {
    private static final int ENTITY_ID = -50000;

    private final RustBuilding plugin;
    private final Map<UUID, HologramData> fakeEntities = new ConcurrentHashMap<>();
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    public void start(Plugin plugin) {
        WrappedDataWatcher.Registry.get(Byte.class); // force registry synchronous init
        executorService.scheduleAtFixedRate(this::runSafe, 0, 50, TimeUnit.MILLISECONDS);
        Bukkit.getPluginManager().registerEvents(new ShowBlocksListener(), plugin);
    }

    private void runSafe() {
        try {
            run();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    private void run() {
        fakeEntities.keySet().removeIf(uuid -> Bukkit.getPlayer(uuid) == null);
        for (val player : Bukkit.getOnlinePlayers()) {
            if (!player.getLocation().isChunkLoaded()) continue;
            val nmsPlayer = ((CraftPlayer) player).getHandle();
            val selectedPattern = PluginUtils.getSelectedPattern(nmsPlayer.getItemInMainHand());
            if (selectedPattern == null) {
                clearEntity(nmsPlayer, fakeEntities.get(player.getUniqueId()));
                continue;
            }

            val newScope = plugin.getApi().findElementPlace(player, selectedPattern);
            val oldHologramData = fakeEntities.get(player.getUniqueId());
            val hologramData = oldHologramData != null ? oldHologramData : createHologramData(player, newScope);
            if (hologramData.scope == newScope) continue;

            hologramData.scope = newScope;
            updateEntityDisplay(player, hologramData);
        }
    }

    public void stop() {
        executorService.shutdown();
    }

    @SneakyThrows
    private HologramData createHologramData(Player player, ElementPlacementScope scope) {
        val entityID = ENTITY_ID; //ThreadLocalRandom.current().nextInt(); // TODO: change generation of id
        val hologramData = new HologramData(entityID, scope, scope.getPatternType(), scope.canBePlaced());
        fakeEntities.put(player.getUniqueId(), hologramData);

        val packet = ProtocolLibrary.getProtocolManager().createPacket(PacketType.Play.Server.SPAWN_ENTITY_LIVING);
        val entityUUID = UUID.randomUUID(); // and maybe UUID
        packet.getIntegers().write(0, entityID);
        packet.getUUIDs().write(0, entityUUID);
        packet.getIntegers().write(1, EntityTypes.b.a(EntityArmorStand.class));
        writeLocationData(packet, scope);
        val compressedYaw = getCompressedYaw(scope);
        packet.getBytes().write(0, compressedYaw);
        packet.getBytes().write(1, (byte) 0); // compressed pitch
        packet.getBytes().write(2, compressedYaw); // head rotation == yaw
        packet.getIntegers().write(2, 0); // motX
        packet.getIntegers().write(3, 0); // motY
        packet.getIntegers().write(4, 0); // motZ
        ProtocolLibrary.getProtocolManager().sendServerPacket(player, packet);

        sendMetadataPacket(player, hologramData, Arrays.asList(
                Tuples.pair(EntityArmorStand.a, (byte) 16),
                Tuples.pair(DataWatcherObjectAccessor.getFlagObject(), (byte) (1 | 1 << 5)) // make entity invisible
        ));
        updateEntityEquipment(player, hologramData, true);

        return hologramData;
    }

    @SneakyThrows
    private void sendMetadataPacket(
            Player player, HologramData hologramData,
            List<Pair<DataWatcherObject<Byte>, Byte>> dataWatcherObjects
    ) {
        val metadataPacket = ProtocolLibrary.getProtocolManager().createPacket(PacketType.Play.Server.ENTITY_METADATA);
        metadataPacket.getIntegers().write(0, hologramData.entityID);
        val serializer = WrappedDataWatcher.Registry.get(Byte.class);
        val wrappedObjects = dataWatcherObjects.stream().map(pair -> {
            val watcherObject = new WrappedDataWatcher.WrappedDataWatcherObject(pair.getOne().a(), serializer);
            val wrappedObject = new WrappedWatchableObject(watcherObject, pair.getTwo());
            wrappedObject.setDirtyState(true);
            return wrappedObject;
        }).collect(Collectors.toList());
        metadataPacket.getWatchableCollectionModifier().write(0, wrappedObjects);
        ProtocolLibrary.getProtocolManager().sendServerPacket(player, metadataPacket);
    }

    private void clearEntity(EntityPlayer player, HologramData hologramData) {
        if (hologramData == null) return;

        val packet = new PacketPlayOutEntityDestroy(hologramData.entityID);
        player.playerConnection.sendPacket(packet);
        fakeEntities.remove(player.getUniqueID());
    }

    @SneakyThrows
    private void updateEntityDisplay(Player player, HologramData hologramData) {
        val packet = ProtocolLibrary.getProtocolManager().createPacket(PacketType.Play.Server.ENTITY_TELEPORT);
        packet.getIntegers().write(0, hologramData.entityID);
        writeLocationData(packet, hologramData.scope);
        packet.getBytes().write(0, getCompressedYaw(hologramData.scope));
        packet.getBytes().write(1, (byte) 0);
        packet.getBooleans().write(0, true);

        ProtocolLibrary.getProtocolManager().sendServerPacket(player, packet);

        updateEntityEquipment(player, hologramData, false);
    }

    @SneakyThrows
    private void updateEntityEquipment(Player player, HologramData hologramData, boolean force) {
        val canBePlaced = hologramData.getScope().canBePlaced();
        if (!force && hologramData.getScope().canBePlaced() == hologramData.canBePlaced &&
                hologramData.getScope().getPatternType() == hologramData.patternType) return;

        val patternType = hologramData.getScope().getPatternType();
        val hologramDisplay = canBePlaced ? patternType.getCorrectHologramDisplay() : patternType.getWrongHologramDisplay();
        val equipmentPacket = ProtocolLibrary.getProtocolManager().createPacket(PacketType.Play.Server.ENTITY_EQUIPMENT);
        equipmentPacket.getIntegers().write(0, hologramData.entityID);
        equipmentPacket.getItemSlots().write(0, EnumWrappers.ItemSlot.HEAD);
        equipmentPacket.getItemModifier().write(0, hologramDisplay);
        ProtocolLibrary.getProtocolManager().sendServerPacket(player, equipmentPacket);

        hologramData.canBePlaced = canBePlaced;
        hologramData.patternType = patternType;
    }

    private void writeLocationData(PacketContainer packet, ElementPlacementScope scope) {
        var centerLocation = scope.getCenter().asBukkitLocation().toCenterLocation();
        packet.getDoubles().write(0, centerLocation.getX());
        packet.getDoubles().write(1, centerLocation.getY() - 1.5);
        packet.getDoubles().write(2, centerLocation.getZ());
    }

    private byte getCompressedYaw(ElementPlacementScope scope) {
        return (byte) ((int) (scope.getRotation().getYaw() * 256.0F / 360.0F));
    }

    class ShowBlocksListener implements Listener {
        @EventHandler
        public void onPlayerQuit(PlayerQuitEvent e) {
            fakeEntities.remove(e.getPlayer().getUniqueId());
        }
    }

    @Data
    @AllArgsConstructor
    private static final class HologramData {
        private final int entityID;
        private ElementPlacementScope scope;
        private PatternType patternType;
        private boolean canBePlaced;
    }

    private abstract static class DataWatcherObjectAccessor extends Entity {
        public DataWatcherObjectAccessor(World world) {
            super(world);
        }

        public static DataWatcherObject<Byte> getFlagObject() {
            return Z;
        }
    }
}
