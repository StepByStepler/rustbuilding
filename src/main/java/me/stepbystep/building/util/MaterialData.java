package me.stepbystep.building.util;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.bukkit.Material;

@Data
@AllArgsConstructor
public class MaterialData {
    private final Material material;
    private final byte data;

    public MaterialData(Material material) {
        this(material, 0);
    }

    public MaterialData(Material material, int data) {
        this(material, (byte) data);
    }
}
