package me.stepbystep.building.util;

import lombok.NonNull;
import org.apache.commons.lang.Validate;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.util.NumberConversions;
import org.bukkit.util.Vector;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

/**
 * A mutable axis aligned bounding box (AABB).
 * <p>
 * This basically represents a rectangular box (specified by minimum and maximum
 * corners) that can for example be used to describe the position and extents of
 * an object (such as an entity, block, or rectangular region) in 3D space. Its
 * edges and faces are parallel to the axes of the cartesian coordinate system.
 * <p>
 * The bounding box may be degenerate (one or more sides having the length 0).
 * <p>
 * Because bounding boxes are mutable, storing them long term may be dangerous
 * if they get modified later. If you want to keep around a bounding box, it may
 * be wise to call {@link #clone()} in order to get a copy.
 */
@SerializableAs("BoundingBox")
public class BoundingBox implements Cloneable, ConfigurationSerializable {
    public static final BoundingBox EMPTY = new BoundingBox(
            Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE,
            Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE
    );

    /**
     * Creates a new bounding box using the coordinates of the given vectors as
     * corners.
     *
     * @param corner1 the first corner
     * @param corner2 the second corner
     * @return the bounding box
     */
    @NonNull
    public static BoundingBox of(@NonNull Vector corner1, @NonNull Vector corner2) {
        Validate.notNull(corner1, "Corner1 is null!");
        Validate.notNull(corner2, "Corner2 is null!");
        return new BoundingBox(corner1.getBlockX(), corner1.getBlockY(), corner1.getBlockZ(), corner2.getBlockX(), corner2.getBlockY(), corner2.getBlockZ());
    }

    /**
     * Creates a new bounding box using the coordinates of the given locations
     * as corners.
     *
     * @param corner1 the first corner
     * @param corner2 the second corner
     * @return the bounding box
     */
    @NonNull
    public static BoundingBox of(@NonNull Location corner1, @NonNull Location corner2) {
        Validate.notNull(corner1, "Corner1 is null!");
        Validate.notNull(corner2, "Corner2 is null!");
        Validate.isTrue(Objects.equals(corner1.getWorld(), corner2.getWorld()), "Locations from different worlds!");
        return new BoundingBox(corner1.getBlockX(), corner1.getBlockY(), corner1.getBlockZ(), corner2.getBlockX(), corner2.getBlockY(), corner2.getBlockZ());
    }

    /**
     * Creates a new bounding box using the coordinates of the given blocks as
     * corners.
     * <p>
     * The bounding box will be sized to fully contain both blocks.
     *
     * @param corner1 the first corner block
     * @param corner2 the second corner block
     * @return the bounding box
     */
    @NonNull
    public static BoundingBox of(@NonNull Block corner1, @NonNull Block corner2) {
        Validate.notNull(corner1, "Corner1 is null!");
        Validate.notNull(corner2, "Corner2 is null!");
        Validate.isTrue(Objects.equals(corner1.getWorld(), corner2.getWorld()), "Blocks from different worlds!");

        int x1 = corner1.getX();
        int y1 = corner1.getY();
        int z1 = corner1.getZ();
        int x2 = corner2.getX();
        int y2 = corner2.getY();
        int z2 = corner2.getZ();

        int minX = Math.min(x1, x2);
        int minY = Math.min(y1, y2);
        int minZ = Math.min(z1, z2);
        int maxX = Math.max(x1, x2) + 1;
        int maxY = Math.max(y1, y2) + 1;
        int maxZ = Math.max(z1, z2) + 1;

        return new BoundingBox(minX, minY, minZ, maxX, maxY, maxZ);
    }

    /**
     * Creates a new 1x1x1 sized bounding box containing the given block.
     *
     * @param block the block
     * @return the bounding box
     */
    @NonNull
    public static BoundingBox of(@NonNull Block block) {
        Validate.notNull(block, "Block is null!");
        return new BoundingBox(block.getX(), block.getY(), block.getZ(), block.getX() + 1, block.getY() + 1, block.getZ() + 1);
    }

    /**
     * Creates a new bounding box using the given center and extents.
     *
     * @param center the center
     * @param x 1/2 the size of the bounding box along the x axis
     * @param y 1/2 the size of the bounding box along the y axis
     * @param z 1/2 the size of the bounding box along the z axis
     * @return the bounding box
     */
    @NonNull
    public static BoundingBox of(@NonNull Vector center, int x, int y, int z) {
        Validate.notNull(center, "Center is null!");
        return new BoundingBox(center.getBlockX() - x, center.getBlockY() - y, center.getBlockZ() - z, center.getBlockX() + x, center.getBlockY() + y, center.getBlockZ() + z);
    }

    /**
     * Creates a new bounding box using the given center and extents.
     *
     * @param center the center
     * @param x 1/2 the size of the bounding box along the x axis
     * @param y 1/2 the size of the bounding box along the y axis
     * @param z 1/2 the size of the bounding box along the z axis
     * @return the bounding box
     */
    @NonNull
    public static BoundingBox of(@NonNull Location center, int x, int y, int z) {
        Validate.notNull(center, "Center is null!");
        return new BoundingBox(center.getBlockX() - x, center.getBlockY() - y, center.getBlockZ() - z, center.getBlockX() + x, center.getBlockY() + y, center.getBlockZ() + z);
    }

    private int minX;
    private int minY;
    private int minZ;
    private int maxX;
    private int maxY;
    private int maxZ;

    /**
     * Creates a new (degenerate) bounding box with all corner coordinates at
     * <code>0</code>.
     */
    public BoundingBox() {
        this.resize(0, 0, 0, 0, 0, 0);
    }

    /**
     * Creates a new bounding box from the given corner coordinates.
     *
     * @param x1 the first corner's x value
     * @param y1 the first corner's y value
     * @param z1 the first corner's z value
     * @param x2 the second corner's x value
     * @param y2 the second corner's y value
     * @param z2 the second corner's z value
     */
    public BoundingBox(int x1, int y1, int z1, int x2, int y2, int z2) {
        this.resize(x1, y1, z1, x2, y2, z2);
    }

    /**
     * Resizes this bounding box.
     *
     * @param x1 the first corner's x value
     * @param y1 the first corner's y value
     * @param z1 the first corner's z value
     * @param x2 the second corner's x value
     * @param y2 the second corner's y value
     * @param z2 the second corner's z value
     * @return this bounding box (resized)
     */
    @NonNull
    public BoundingBox resize(int x1, int y1, int z1, int x2, int y2, int z2) {
        NumberConversions.checkFinite(x1, "x1 not finite");
        NumberConversions.checkFinite(y1, "y1 not finite");
        NumberConversions.checkFinite(z1, "z1 not finite");
        NumberConversions.checkFinite(x2, "x2 not finite");
        NumberConversions.checkFinite(y2, "y2 not finite");
        NumberConversions.checkFinite(z2, "z2 not finite");

        this.minX = Math.min(x1, x2);
        this.minY = Math.min(y1, y2);
        this.minZ = Math.min(z1, z2);
        this.maxX = Math.max(x1, x2);
        this.maxY = Math.max(y1, y2);
        this.maxZ = Math.max(z1, z2);
        return this;
    }

    /**
     * Gets the minimum x value.
     *
     * @return the minimum x value
     */
    public int getMinX() {
        return minX;
    }

    /**
     * Gets the minimum y value.
     *
     * @return the minimum y value
     */
    public int getMinY() {
        return minY;
    }

    /**
     * Gets the minimum z value.
     *
     * @return the minimum z value
     */
    public int getMinZ() {
        return minZ;
    }

    /**
     * Gets the minimum corner as vector.
     *
     * @return the minimum corner as vector
     */
    @NonNull
    public Vector getMin() {
        return new Vector(minX, minY, minZ);
    }

    /**
     * Gets the maximum x value.
     *
     * @return the maximum x value
     */
    public int getMaxX() {
        return maxX;
    }

    /**
     * Gets the maximum y value.
     *
     * @return the maximum y value
     */
    public int getMaxY() {
        return maxY;
    }

    /**
     * Gets the maximum z value.
     *
     * @return the maximum z value
     */
    public int getMaxZ() {
        return maxZ;
    }

    /**
     * Gets the maximum corner as vector.
     *
     * @return the maximum corner vector
     */
    @NonNull
    public Vector getMax() {
        return new Vector(maxX, maxY, maxZ);
    }

    /**
     * Gets the width of the bounding box in the x direction.
     *
     * @return the width in the x direction
     */
    public int getWidthX() {
        return (this.maxX - this.minX);
    }

    /**
     * Gets the width of the bounding box in the z direction.
     *
     * @return the width in the z direction
     */
    public int getWidthZ() {
        return (this.maxZ - this.minZ);
    }

    /**
     * Gets the height of the bounding box.
     *
     * @return the height
     */
    public int getHeight() {
        return (this.maxY - this.minY);
    }

    /**
     * Gets the volume of the bounding box.
     *
     * @return the volume
     */
    public int getVolume() {
        return (this.getHeight() * this.getWidthX() * this.getWidthZ());
    }

    /**
     * Copies another bounding box.
     *
     * @param other the other bounding box
     * @return this bounding box
     */
    @NonNull
    public BoundingBox copy(@NonNull BoundingBox other) {
        Validate.notNull(other, "Other bounding box is null!");
        return this.resize(other.getMinX(), other.getMinY(), other.getMinZ(), other.getMaxX(), other.getMaxY(), other.getMaxZ());
    }

    /**
     * Expands this bounding box to contain (or border) the specified position.
     *
     * @param posX the x position value
     * @param posY the y position value
     * @param posZ the z position value
     * @return this bounding box (now expanded)
     * @see #contains(int, int, int)
     */
    @NonNull
    public BoundingBox union(int posX, int posY, int posZ) {
        int newMinX = Math.min(this.minX, posX);
        int newMinY = Math.min(this.minY, posY);
        int newMinZ = Math.min(this.minZ, posZ);
        int newMaxX = Math.max(this.maxX, posX);
        int newMaxY = Math.max(this.maxY, posY);
        int newMaxZ = Math.max(this.maxZ, posZ);
        if (newMinX == this.minX && newMinY == this.minY && newMinZ == this.minZ && newMaxX == this.maxX && newMaxY == this.maxY && newMaxZ == this.maxZ) {
            return this;
        }
        return this.resize(newMinX, newMinY, newMinZ, newMaxX, newMaxY, newMaxZ);
    }

    /**
     * Expands this bounding box to contain (or border) the specified position.
     *
     * @param position the position
     * @return this bounding box (now expanded)
     * @see #contains(int, int, int)
     */
    @NonNull
    public BoundingBox union(@NonNull Vector position) {
        Validate.notNull(position, "Position is null!");
        return this.union(position.getBlockX(), position.getBlockY(), position.getBlockZ());
    }

    /**
     * Expands this bounding box to contain (or border) the specified position.
     *
     * @param position the position
     * @return this bounding box (now expanded)
     * @see #contains(int, int, int)
     */
    @NonNull
    public BoundingBox union(@NonNull Location position) {
        Validate.notNull(position, "Position is null!");
        return this.union(position.getBlockX(), position.getBlockY(), position.getBlockZ());
    }

    /**
     * Expands this bounding box to contain both this and the given bounding
     * box.
     *
     * @param other the other bounding box
     * @return this bounding box (now expanded)
     */
    @NonNull
    public BoundingBox union(@NonNull BoundingBox other) {
        Validate.notNull(other, "Other bounding box is null!");
        if (this.contains(other)) return this;
        return unionWithoutCheck(other);
    }

    /**
     * Expands this bounding box to contain both this and the given bounding
     * box without checking if this bounding box contains given bounding box
     *
     * @param other the other bounding box
     * @return this bounding box (now expanded)
     */
    @NonNull
    public BoundingBox unionWithoutCheck(@NonNull BoundingBox other) {
        Validate.notNull(other, "Other bounding box is null!");
        minX = Math.min(minX, other.minX);
        minY = Math.min(minY, other.minY);
        minZ = Math.min(minZ, other.minZ);
        maxX = Math.max(maxX, other.maxX);
        maxY = Math.max(maxY, other.maxY);
        maxZ = Math.max(maxZ, other.maxZ);
        return this;
    }

    /**
     * Resizes this bounding box to represent the intersection of this and the
     * given bounding box.
     *
     * @param other the other bounding box
     * @return this bounding box (now representing the intersection)
     * @throws IllegalArgumentException if the bounding boxes don't overlap
     */
    @NonNull
    public BoundingBox intersection(@NonNull BoundingBox other) {
        Validate.notNull(other, "Other bounding box is null!");
        Validate.isTrue(this.overlaps(other), "The bounding boxes do not overlap!");
        int newMinX = Math.max(this.minX, other.minX);
        int newMinY = Math.max(this.minY, other.minY);
        int newMinZ = Math.max(this.minZ, other.minZ);
        int newMaxX = Math.min(this.maxX, other.maxX);
        int newMaxY = Math.min(this.maxY, other.maxY);
        int newMaxZ = Math.min(this.maxZ, other.maxZ);
        return this.resize(newMinX, newMinY, newMinZ, newMaxX, newMaxY, newMaxZ);
    }

    /**
     * Shifts this bounding box by the given amounts.
     *
     * @param shiftX the shift in x direction
     * @param shiftY the shift in y direction
     * @param shiftZ the shift in z direction
     * @return this bounding box (now shifted)
     */
    @NonNull
    public BoundingBox shift(int shiftX, int shiftY, int shiftZ) {
        if (shiftX == 0.0D && shiftY == 0.0D && shiftZ == 0.0D) return this;
        return this.resize(this.minX + shiftX, this.minY + shiftY, this.minZ + shiftZ,
                this.maxX + shiftX, this.maxY + shiftY, this.maxZ + shiftZ);
    }

    /**
     * Shifts this bounding box by the given amounts.
     *
     * @param shift the shift
     * @return this bounding box (now shifted)
     */
    @NonNull
    public BoundingBox shift(@NonNull Vector shift) {
        Validate.notNull(shift, "Shift is null!");
        return this.shift(shift.getBlockX(), shift.getBlockY(), shift.getBlockZ());
    }

    /**
     * Shifts this bounding box by the given amounts.
     *
     * @param shift the shift
     * @return this bounding box (now shifted)
     */
    @NonNull
    public BoundingBox shift(@NonNull Location shift) {
        Validate.notNull(shift, "Shift is null!");
        return this.shift(shift.getBlockX(), shift.getBlockY(), shift.getBlockZ());
    }

    private boolean overlaps(int minX, int minY, int minZ, int maxX, int maxY, int maxZ) {
        return this.minX < maxX && this.maxX > minX
                && this.minY < maxY && this.maxY > minY
                && this.minZ < maxZ && this.maxZ > minZ;
    }

    /**
     * Checks if this bounding box overlaps with the given bounding box.
     * <p>
     * Bounding boxes that are only intersecting at the borders are not
     * considered overlapping.
     *
     * @param other the other bounding box
     * @return <code>true</code> if overlapping
     */
    public boolean overlaps(@NonNull BoundingBox other) {
        Validate.notNull(other, "Other bounding box is null!");
        return this.overlaps(other.minX, other.minY, other.minZ, other.maxX, other.maxY, other.maxZ);
    }

    /**
     * Checks if this bounding box overlaps with the bounding box that is
     * defined by the given corners.
     * <p>
     * Bounding boxes that are only intersecting at the borders are not
     * considered overlapping.
     *
     * @param min the first corner
     * @param max the second corner
     * @return <code>true</code> if overlapping
     */
    public boolean overlaps(@NonNull Vector min, @NonNull Vector max) {
        Validate.notNull(min, "Min is null!");
        Validate.notNull(max, "Max is null!");
        int x1 = min.getBlockX();
        int y1 = min.getBlockY();
        int z1 = min.getBlockZ();
        int x2 = max.getBlockX();
        int y2 = max.getBlockY();
        int z2 = max.getBlockZ();
        return this.overlaps(Math.min(x1, x2), Math.min(y1, y2), Math.min(z1, z2),
                Math.max(x1, x2), Math.max(y1, y2), Math.max(z1, z2));
    }

    /**
     * Checks if this bounding box contains the specified position.
     * <p>
     * Positions exactly on the minimum borders of the bounding box are
     * considered to be inside the bounding box, while positions exactly on the
     * maximum borders are considered to be outside. This allows bounding boxes
     * to reside directly next to each other with positions always only residing
     * in exactly one of them.
     *
     * @param x the position's x coordinates
     * @param y the position's y coordinates
     * @param z the position's z coordinates
     * @return <code>true</code> if the bounding box contains the position
     */
    public boolean contains(int x, int y, int z) {
        return x >= this.minX && x < this.maxX
                && y >= this.minY && y < this.maxY
                && z >= this.minZ && z < this.maxZ;
    }

    /**
     * Checks if this bounding box contains the specified position.
     * <p>
     * Positions exactly on the minimum borders of the bounding box are
     * considered to be inside the bounding box, while positions exactly on the
     * maximum borders are considered to be outside. This allows bounding boxes
     * to reside directly next to each other with positions always only residing
     * in exactly one of them.
     *
     * @param position the position
     * @return <code>true</code> if the bounding box contains the position
     */
    public boolean contains(@NonNull Vector position) {
        Validate.notNull(position, "Position is null!");
        return this.contains(position.getBlockX(), position.getBlockY(), position.getBlockZ());
    }

    private boolean contains(int minX, int minY, int minZ, int maxX, int maxY, int maxZ) {
        return this.minX <= minX && this.maxX >= maxX
                && this.minY <= minY && this.maxY >= maxY
                && this.minZ <= minZ && this.maxZ >= maxZ;
    }

    /**
     * Checks if this bounding box fully contains the given bounding box.
     *
     * @param other the other bounding box
     * @return <code>true</code> if the bounding box contains the given bounding
     * box
     */
    public boolean contains(@NonNull BoundingBox other) {
        Validate.notNull(other, "Other bounding box is null!");
        return this.contains(other.minX, other.minY, other.minZ, other.maxX, other.maxY, other.maxZ);
    }

    /**
     * Checks if this bounding box fully contains the bounding box that is
     * defined by the given corners.
     *
     * @param min the first corner
     * @param max the second corner
     * @return <code>true</code> if the bounding box contains the specified
     *     bounding box
     */
    public boolean contains(@NonNull Vector min, @NonNull Vector max) {
        Validate.notNull(min, "Min is null!");
        Validate.notNull(max, "Max is null!");
        int x1 = min.getBlockX();
        int y1 = min.getBlockY();
        int z1 = min.getBlockZ();
        int x2 = max.getBlockX();
        int y2 = max.getBlockY();
        int z2 = max.getBlockZ();
        return this.contains(Math.min(x1, x2), Math.min(y1, y2), Math.min(z1, z2),
                Math.max(x1, x2), Math.max(y1, y2), Math.max(z1, z2));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        long temp;
        temp = maxX;
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = maxY;
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = maxZ;
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = minX;
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = minY;
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = minZ;
        result = prime * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof BoundingBox)) return false;
        BoundingBox other = (BoundingBox) obj;
        if (maxX != other.maxX) return false;
        if (maxY != other.maxY) return false;
        if (maxZ != other.maxZ) return false;
        if (minX != other.minX) return false;
        if (minY != other.minY) return false;
        if (minZ != other.minZ) return false;
        return true;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("BoundingBox [minX=");
        builder.append(minX);
        builder.append(", minY=");
        builder.append(minY);
        builder.append(", minZ=");
        builder.append(minZ);
        builder.append(", maxX=");
        builder.append(maxX);
        builder.append(", maxY=");
        builder.append(maxY);
        builder.append(", maxZ=");
        builder.append(maxZ);
        builder.append("]");
        return builder.toString();
    }

    /**
     * Creates a copy of this bounding box.
     *
     * @return the cloned bounding box
     */
    @NonNull
    @Override
    public BoundingBox clone() {
        try {
            return (BoundingBox) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new Error(e);
        }
    }

    @NonNull
    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> result = new LinkedHashMap<String, Object>();
        result.put("minX", minX);
        result.put("minY", minY);
        result.put("minZ", minZ);
        result.put("maxX", maxX);
        result.put("maxY", maxY);
        result.put("maxZ", maxZ);
        return result;
    }

    @NonNull
    public static BoundingBox deserialize(@NonNull Map<String, Object> args) {
        int minX = 0;
        int minY = 0;
        int minZ = 0;
        int maxX = 0;
        int maxY = 0;
        int maxZ = 0;

        if (args.containsKey("minX")) {
            minX = ((Number) args.get("minX")).intValue();
        }
        if (args.containsKey("minY")) {
            minY = ((Number) args.get("minY")).intValue();
        }
        if (args.containsKey("minZ")) {
            minZ = ((Number) args.get("minZ")).intValue();
        }
        if (args.containsKey("maxX")) {
            maxX = ((Number) args.get("maxX")).intValue();
        }
        if (args.containsKey("maxY")) {
            maxY = ((Number) args.get("maxY")).intValue();
        }
        if (args.containsKey("maxZ")) {
            maxZ = ((Number) args.get("maxZ")).intValue();
        }

        return new BoundingBox(minX, minY, minZ, maxX, maxY, maxZ);
    }
}
