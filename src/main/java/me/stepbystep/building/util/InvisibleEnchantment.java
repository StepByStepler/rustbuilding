package me.stepbystep.building.util;

import lombok.SneakyThrows;
import lombok.val;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.inventory.ItemStack;

public class InvisibleEnchantment extends Enchantment {

    public static final InvisibleEnchantment INSTANCE = new InvisibleEnchantment();

    @SneakyThrows
    private InvisibleEnchantment() {
        super(255);
        val acceptEnchantsField = Enchantment.class.getDeclaredField("acceptingNew");
        acceptEnchantsField.setAccessible(true);
        acceptEnchantsField.set(null, true);
        registerEnchantment(this);
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public int getMaxLevel() {
        return 1;
    }

    @Override
    public int getStartLevel() {
        return 0;
    }

    @Override
    public EnchantmentTarget getItemTarget() {
        return null;
    }

    @Override
    public boolean isTreasure() {
        return false;
    }

    @Override
    public boolean isCursed() {
        return false;
    }

    @Override
    public boolean conflictsWith(Enchantment enchantment) {
        return false;
    }

    @Override
    public boolean canEnchantItem(ItemStack itemStack) {
        return false;
    }
}
