package me.stepbystep.building.util;

import lombok.Data;

@Data
public class IntTriple {
    private final int first;
    private final int second;
    private final int third;
}
