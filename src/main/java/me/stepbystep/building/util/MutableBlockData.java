package me.stepbystep.building.util;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableTable;
import com.google.common.collect.Maps;
import lombok.val;
import net.minecraft.server.v1_12_R1.*;

import javax.annotation.Nullable;
import java.util.*;

public class MutableBlockData extends BlockDataAbstract {
    private final Block a;
    private final ImmutableMap<IBlockState<?>, Comparable<?>> b;
    private ImmutableTable<IBlockState<?>, Comparable<?>, IBlockData> c;

    public MutableBlockData(Block var1, ImmutableMap<IBlockState<?>, Comparable<?>> var2) {
        a = var1;
        b = var2;
    }

    public Collection<IBlockState<?>> s() {
        return Collections.unmodifiableCollection(b.keySet());
    }

    public <T extends Comparable<T>> T get(IBlockState<T> var1) {
        Comparable<?> var2 = b.get(var1);
        if (var2 == null) {
            throw new IllegalArgumentException("Cannot get property " + var1 + " as it does not exist in " + a.s());
        } else {
            return var1.b().cast(var2);
        }
    }

    public <T extends Comparable<T>, V extends T> IBlockData set(IBlockState<T> var1, V var2) {
        Comparable<?> var3 = b.get(var1);
        if (var3 == null) {
            throw new IllegalArgumentException("Cannot set property " + var1 + " as it does not exist in " + a.s());
        } else if (var3 == var2) {
            return this;
        } else {
            IBlockData var4 = c.get(var1, var2);
            if (var4 == null) {
                throw new IllegalArgumentException("Cannot set property " + var1 + " to " + var2 + " on block " + Block.REGISTRY.b(a) + ", it is not an allowed value");
            } else {
                return var4;
            }
        }
    }

    public ImmutableMap<IBlockState<?>, Comparable<?>> t() {
        return b;
    }

    public Block getBlock() {
        return a;
    }

    public boolean equals(Object var1) {
        return this == var1;
    }

    public int hashCode() {
        return b.hashCode();
    }

    public void a(Map<Map<IBlockState<?>, Comparable<?>>, MutableBlockData> var1) {
        if (c != null) {
            throw new IllegalStateException();
        } else {
            val var2 = HashBasedTable.<IBlockState<?>, Comparable<?>, IBlockData>create();

            for (val iBlockStateComparableEntry : b.entrySet()) {
                val var5 = iBlockStateComparableEntry.getKey();

                for (Comparable<?> comparable : var5.c()) {
                    if (comparable != iBlockStateComparableEntry.getValue()) {
                        var2.put(var5, comparable, var1.get(b(var5, comparable)));
                    }
                }
            }

            c = ImmutableTable.copyOf(var2);
        }
    }

    private Map<IBlockState<?>, Comparable<?>> b(IBlockState<?> var1, Comparable<?> var2) {
        val var3 = Maps.newHashMap(b);
        var3.put(var1, var2);
        return var3;
    }

    public Material getMaterial() {
        return a.q(this);
    }

    public boolean b() {
        return a.l(this);
    }

    public boolean a(Entity var1) {
        return a.a(this, var1);
    }

    public int c() {
        return a.m(this);
    }

    public int d() {
        return a.o(this);
    }

    public boolean f() {
        return a.p(this);
    }

    public MaterialMapColor a(IBlockAccess var1, BlockPosition var2) {
        return a.c(this, var1, var2);
    }

    public IBlockData a(EnumBlockRotation var1) {
        return a.a(this, var1);
    }

    public IBlockData a(EnumBlockMirror var1) {
        return a.a(this, var1);
    }

    public boolean g() {
        return a.c(this);
    }

    public EnumRenderType i() {
        return a.a(this);
    }

    public boolean k() {
        return a.r(this);
    }

    public boolean l() {
        return a.isOccluding(this);
    }

    public boolean m() {
        return a.isPowerSource(this);
    }

    public int a(IBlockAccess var1, BlockPosition var2, EnumDirection var3) {
        return a.b(this, var1, var2, var3);
    }

    public boolean n() {
        return a.isComplexRedstone(this);
    }

    public int a(World var1, BlockPosition var2) {
        return a.c(this, var1, var2);
    }

    public float b(World var1, BlockPosition var2) {
        return a.a(this, var1, var2);
    }

    public float a(EntityHuman var1, World var2, BlockPosition var3) {
        return a.getDamage(this, var1, var2, var3);
    }

    public int b(IBlockAccess var1, BlockPosition var2, EnumDirection var3) {
        return a.c(this, var1, var2, var3);
    }

    public EnumPistonReaction o() {
        return a.h(this);
    }

    public IBlockData c(IBlockAccess var1, BlockPosition var2) {
        return a.updateState(this, var1, var2);
    }

    public boolean p() {
        return a.b(this);
    }

    @Nullable
    public AxisAlignedBB d(IBlockAccess var1, BlockPosition var2) {
        return a.a(this, var1, var2);
    }

    public void a(World var1, BlockPosition var2, AxisAlignedBB var3, List<AxisAlignedBB> var4, @Nullable Entity var5, boolean var6) {
        a.a(this, var1, var2, var3, var4, var5, var6);
    }

    public AxisAlignedBB e(IBlockAccess var1, BlockPosition var2) {
        return a.b(this, var1, var2);
    }

    public MovingObjectPosition a(World var1, BlockPosition var2, Vec3D var3, Vec3D var4) {
        return a.a(this, var1, var2, var3, var4);
    }

    public boolean q() {
        return a.k(this);
    }

    public Vec3D f(IBlockAccess var1, BlockPosition var2) {
        return a.f(this, var1, var2);
    }

    public boolean a(World var1, BlockPosition var2, int var3, int var4) {
        return a.a(this, var1, var2, var3, var4);
    }

    public void doPhysics(World var1, BlockPosition var2, Block var3, BlockPosition var4) {
        a.a(this, var1, var2, var3, var4);
    }

    public boolean r() {
        return a.t(this);
    }

    public EnumBlockFaceShape d(IBlockAccess var1, BlockPosition var2, EnumDirection var3) {
        return a.a(var1, this, var2, var3);
    }
}
