package me.stepbystep.building.util;

import lombok.Data;

@Data
public class IntPair {
    private final int x;
    private final int y;
}
