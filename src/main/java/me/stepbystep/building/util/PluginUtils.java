package me.stepbystep.building.util;

import lombok.val;
import lombok.var;
import me.stepbystep.building.obj.BlockLocation;
import me.stepbystep.building.obj.PatternBlock;
import me.stepbystep.building.pattern.PatternType;
import net.minecraft.server.v1_12_R1.ItemStack;
import org.apache.commons.lang3.tuple.Pair;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.*;
import java.util.function.Function;

public class PluginUtils {
    public static final int LEVELS_COUNT = 5;
    public static final int MAX_LEVEL = LEVELS_COUNT - 1;
    public static final String WORLD_NAME = "RustMe";
    public static final String BUILD_ITEM_TAG = "buildItem";
    public static final String BREAK_ITEM_TAG = "breakItem";
    private static final String SELECTED_PATTERN_TAG = "patternType";
    public static final IntTriple[] blockNeighbours = new IntTriple[] {
            new IntTriple(-1, 0, 0),
            new IntTriple(1, 0, 0),
            new IntTriple(0, -1, 0),
            new IntTriple(0, 1, 0),
            new IntTriple(0, 0, -1),
            new IntTriple(0, 0, 1),
    };

    // TODO: implement ray tracing searching for proper element placement
    public static Location findCenterLocation(Player player, PatternType patternType) {
        val modifier = patternType.isKindOf(PatternType.WALL) ? 3.0 : 5.0;
        val result = player.getEyeLocation().add(player.getLocation().getDirection().multiply(modifier));
        if (patternType == PatternType.ROOF) {
            result.setY(result.getY() - 1.65);
        } else {
            result.setY(result.getY() - 0.65);
        }
        return result;
    }

    public static PatternType getSelectedPattern(ItemStack itemStack) {
        if (itemStack.isEmpty()) return null;

        val tag = itemStack.getTag();
        if (tag == null) return null;
        val patternName = tag.getString(SELECTED_PATTERN_TAG);
        if (patternName == null || patternName.isEmpty()) return null;

        return PatternType.valueOf(patternName);
    }

    public static void setSelectedPattern(ItemStack itemStack, PatternType patternType) {
        var tag = itemStack.getTag();
        if (tag == null || !tag.getBoolean(PluginUtils.BUILD_ITEM_TAG)) return;

        tag.setString(SELECTED_PATTERN_TAG, patternType.getName());
    }

    public static boolean noElementIntersection(Collection<PatternBlock> blocks1, Collection<PatternBlock> blocks2) {
        for (val block1 : blocks1) {
            val element1 = block1.getBuildingElement();
            for (val block2 : blocks2) {
                if (block2.getBuildingElement() == element1) return false;
            }
        }
        return true;
    }

    public static <T, R> List<Pair<T, R>> group(Iterable<T> it1, Iterable<R> it2) {
        val result = new ArrayList<Pair<T, R>>();
        for (val el1 : it1) {
            for (val el2 : it2) {
                result.add(Pair.of(el1, el2));
            }
        }

        return result;
    }

    public static List<IntPair> group(int minX, int maxX, int minY, int maxY) {
        val result = new ArrayList<IntPair>();
        for (int x = minX; x <= maxX; x++) {
            for (int y = minY; y <= maxY; y++) {
                result.add(new IntPair(x, y));
            }
        }
        return result;
    }

    public static <K, V> Map<K, V> associate(Iterable<K> iterable, Function<K, V> mapper) {
        val result = new LinkedHashMap<K, V>();
        for (val key : iterable) {
            result.put(key, mapper.apply(key));
        }
        return result;
    }

    public static <K, V> Map<K, V> concatMaps(Map<K, V> map1, Map<K, V> map2) {
        val result = new LinkedHashMap<K, V>(map1.size() + map2.size());
        result.putAll(map1);
        result.putAll(map2);
        return result;
    }

    public static <T, R extends Comparable<R>> R maxOf(Iterable<T> iterable, Function<T, R> selector) {
        val iterator = iterable.iterator();
        if (!iterator.hasNext()) throw new NoSuchElementException();
        var maxValue = selector.apply(iterator.next());
        while (iterator.hasNext()) {
            val v = selector.apply(iterator.next());
            if (maxValue.compareTo(v) < 0) {
                maxValue = v;
            }
        }
        return maxValue;
    }

    public static <T, R extends Comparable<R>> R minOf(Iterable<T> iterable, Function<T, R> selector) {
        val iterator = iterable.iterator();
        if (!iterator.hasNext()) throw new NoSuchElementException();
        var maxValue = selector.apply(iterator.next());
        while (iterator.hasNext()) {
            val v = selector.apply(iterator.next());
            if (maxValue.compareTo(v) > 0) {
                maxValue = v;
            }
        }
        return maxValue;
    }

    public static <T> BoundingBox computeBoundingBox(Collection<T> blocks, Function<T, BlockLocation> locFunction) {
        int minX = Integer.MAX_VALUE, minZ = Integer.MAX_VALUE;
        int maxX = Integer.MIN_VALUE, maxZ = Integer.MIN_VALUE;

        for (val block : blocks) {
            val blockLocation = locFunction.apply(block);
            if (blockLocation.getX() < minX) minX = blockLocation.getX();
            if (blockLocation.getX() > maxX) maxX = blockLocation.getX();
            if (blockLocation.getZ() < minZ) minZ = blockLocation.getZ();
            if (blockLocation.getZ() > maxZ) maxZ = blockLocation.getZ();
        }

        return new BoundingBox(minX - 5, 0, minZ - 5, maxX + 6, 255, maxZ + 6);
    }
}
