package me.stepbystep.building;

import com.google.common.collect.ImmutableMap;
import lombok.AllArgsConstructor;
import lombok.val;
import me.stepbystep.building.util.MaterialData;
import me.stepbystep.building.util.MutableBlockData;
import me.stepbystep.building.util.PluginUtils;
import net.minecraft.server.v1_12_R1.*;
import org.bukkit.Material;
import org.bukkit.TreeSpecies;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
public enum BlockType {
    BORDER(Collections.nCopies(PluginUtils.LEVELS_COUNT, new MaterialData(Material.EMERALD_BLOCK))),
    INNER(Arrays.asList(
            new MaterialData(Material.HAY_BLOCK),
            new MaterialData(Material.WOOD, TreeSpecies.REDWOOD.getData()),
            new MaterialData(Material.COAL_ORE),
            new MaterialData(Material.IRON_BLOCK),
            new MaterialData(Material.DIAMOND_BLOCK)
    )),
    FAKE_BASEMENT_INNER(
            INNER.levels.stream().map(materialData -> {
                val material = materialData.getMaterial();
                if (material == Material.WOOD || material == Material.HAY_BLOCK)
                    return new MaterialData(Material.AIR);
                else
                    return materialData;
            }).collect(Collectors.toList())
    ),
    AIR(Collections.nCopies(PluginUtils.LEVELS_COUNT, new MaterialData(Material.AIR))),
    FRAME_LEFT(Arrays.asList(
            new MaterialData(Material.WHITE_SHULKER_BOX),
            new MaterialData(Material.PURPLE_SHULKER_BOX),
            new MaterialData(Material.YELLOW_SHULKER_BOX),
            new MaterialData(Material.PINK_SHULKER_BOX),
            new MaterialData(Material.GRAY_SHULKER_BOX)
    )) {
        @Override
        public void updateState(Block block, RotationQuarter rotation) {
            WorldServer world = ((CraftWorld) block.getWorld()).getHandle();
            BlockPosition blockPosition = new BlockPosition(block.getX(), block.getY(), block.getZ());
            IBlockData blockData = world.getType(blockPosition);
            EnumDirection direction;
            switch (rotation) {
                case FIRST:
                    direction = EnumDirection.EAST;
                    break;
                case SECOND:
                    direction = EnumDirection.NORTH;
                    break;
                case THIRD:
                    direction = EnumDirection.WEST;
                    break;
                case FOURTH:
                    direction = EnumDirection.SOUTH;
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + rotation);
            }

            ImmutableMap<IBlockState<?>, Comparable<?>> blockStates = ImmutableMap.<IBlockState<?>, Comparable<?>>builder()
                    .put(BlockShulkerBox.a, direction)
                    .build();
            world.setTypeUpdate(blockPosition, new MutableBlockData(blockData.getBlock(), blockStates));
        }
    },
    FRAME_TOP(Arrays.asList(
            new MaterialData(Material.ORANGE_SHULKER_BOX),
            new MaterialData(Material.LIGHT_BLUE_SHULKER_BOX),
            new MaterialData(Material.GREEN_SHULKER_BOX),
            new MaterialData(Material.BLACK_SHULKER_BOX),
            new MaterialData(Material.CYAN_SHULKER_BOX)
    )) {
        @Override
        public int getPriority() {
            return 1;
        }

        @Override
        public void updateState(Block block, RotationQuarter rotation) {
//            WorldServer world = ((CraftWorld) block.getWorld()).getHandle();
//            BlockPosition blockPosition = new BlockPosition(block.getX(), block.getY(), block.getZ());
//            IBlockData blockData = world.getType(blockPosition);
//            blockData.set(BlockShulkerBox.a, EnumDirection.DOWN);
//            world.setTypeUpdate(blockPosition, blockData);
        }
    },
    ;

    private final List<MaterialData> levels;

    public MaterialData getMaterialData(int level) {
        return levels.get(level);
    }

    public void applyTo(Block block, int level, RotationQuarter rotation) {
        val materialData = getMaterialData(level);
        block.setType(materialData.getMaterial());
        block.setData(materialData.getData());
        updateState(block, rotation);
    }

    public void updateState(Block block, RotationQuarter rotation) {
        // nothing
    }

    public int getPriority() {
        return 0;
    }
}
