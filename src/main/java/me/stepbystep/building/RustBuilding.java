package me.stepbystep.building;

import lombok.Getter;
import me.stepbystep.building.api.RustBuildingApi;
import me.stepbystep.building.db.BlockStorage;
import me.stepbystep.building.impl.RustBuildingApiImpl;
import me.stepbystep.building.util.PluginUtils;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.WorldLoadEvent;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.java.JavaPluginLoader;

import java.io.File;

@Getter
public class RustBuilding extends JavaPlugin implements Listener {
    public static RustBuilding INSTANCE;

    private BlockStorage blockStorage;
    private RustBuildingApi api;
    private ShowBlocksRunnable showBlocksRunnable;

    public RustBuilding() {
        super();
    }

    protected RustBuilding(JavaPluginLoader loader, PluginDescriptionFile description, File dataFolder, File file) {
        super(loader, description, dataFolder, file);
    }

    @Override
    public void onEnable() {
        INSTANCE = this;
        getServer().getPluginManager().registerEvents(this, this);

        if (Bukkit.getWorld(PluginUtils.WORLD_NAME) != null) {
            enablePlugin();
        }
    }

    @Override
    public void onDisable() {
        blockStorage.save();
        showBlocksRunnable.stop();
    }

    @EventHandler
    public void onWorldLoad(WorldLoadEvent e) {
        if (blockStorage != null || !e.getWorld().getName().equals(PluginUtils.WORLD_NAME)) return;

        enablePlugin();
    }

    private void enablePlugin() {
        blockStorage = new BlockStorage(this);
        api = new RustBuildingApiImpl(this);
        showBlocksRunnable = new ShowBlocksRunnable(this);
        showBlocksRunnable.start(this);
        getServer().getPluginManager().registerEvents(new RustBuildingListener(this), this);

//        getCommand("giveitem").setExecutor(new GiveItemCommand());
//        getCommand("upgrade").setExecutor(new UpgradeBlockCommand());
    }
}
