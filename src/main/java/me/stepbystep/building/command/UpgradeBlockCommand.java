package me.stepbystep.building.command;

import lombok.val;
import me.stepbystep.building.RustBuilding;
import me.stepbystep.building.obj.BlockLocation;
import me.stepbystep.building.pattern.PatternType;
import me.stepbystep.building.util.PluginUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

public class UpgradeBlockCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Эта команда только для игроков");
            return true;
        }
        val player = (Player) sender;
        val targetBlock = player.getTargetBlock(null, 5);
        if (targetBlock.getType() == Material.AIR) {
            sender.sendMessage(ChatColor.RED + "Этот блок не является постройкой");
            return true;
        }

        val blockStorage = RustBuilding.INSTANCE.getBlockStorage();
        val patternBlocks = blockStorage.getAllPatternBlocks(BlockLocation.fromBlock(targetBlock));
        if (patternBlocks.isEmpty()) {
            sender.sendMessage(ChatColor.RED + "Этот блок не является постройкой");
            return true;
        }
        if (patternBlocks.size() != 1) {
            sender.sendMessage(ChatColor.RED + "Выберите блок, который принадлежит только одной постройке");
            return true;
        }

        val patternBlock = patternBlocks.getAny();
        val buildingElement = patternBlock.getBuildingElement();
        val newLevel = buildingElement.getLevel() + 1;
        if (newLevel == PluginUtils.LEVELS_COUNT) {
            sender.sendMessage(ChatColor.RED + "Этот элемент прокачан до максимального уровня");
            return true;
        }
        
//        val centerEntries = patternBlocks.getCenterBlocks().entrySet();
//
//        val centerEntry = Iterables.getOnlyElement(centerEntries);
//        upgradeBlocks(centerEntry.getValue(), centerEntry.getKey(), patternBlocks.getPatternType(), newLevel);

        RustBuilding.INSTANCE.getApi().upgradeElement(buildingElement, newLevel);
        
        return true;
    }

    private void upgradeBlocks(UUID blockUUID, BlockLocation center, PatternType patternType, int newLevel) {
//        val blockStorage = RustBuilding.INSTANCE.getBlockStorage();
//
//        for (int x = patternType.getMinXDiff(); x <= patternType.getMaxXDiff(); x++) {
//            for (int y = patternType.getMinYDiff(); y <= patternType.getMaxYDiff(); y++) {
//                for (int z = patternType.getMinZDiff(); z <= patternType.getMaxZDiff(); z++) {
//                    val block = center.getBlock(x, y, z);
//                    val patternBlock = blockStorage.getAnyPatternBlock(block);
//                    if (patternBlock == null) continue;
//                    if (patternBlock.getLevel() >= newLevel) continue;
//                    if (!blockUUID.equals(patternBlock.getCenterBlocks().get(center))) continue;
//
//                    val materialData = patternBlock.getBlockType().getMaterialData(newLevel);
//                    block.setType(materialData.getMaterial());
//                    block.setData(materialData.getData());
//                    patternBlock.setLevel(newLevel);
//                }
//            }
//        }
    }
}
