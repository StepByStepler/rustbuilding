package me.stepbystep.building.command;

import lombok.val;
import me.stepbystep.building.util.PluginUtils;
import net.minecraft.server.v1_12_R1.ItemStack;
import net.minecraft.server.v1_12_R1.Items;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;

public class GiveItemCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Эта команда доступна только игрокам");
            return true;
        }
        val player = (Player) sender;

        val planStack = new ItemStack(Items.PAPER);
        val planBukkitStack = CraftItemStack.asCraftMirror(planStack);
        val planMeta = planBukkitStack.getItemMeta();
        planMeta.setDisplayName(ChatColor.GREEN + "Постройка");
        planBukkitStack.setItemMeta(planMeta);
        planStack.getTag().setBoolean(PluginUtils.BUILD_ITEM_TAG, true);
        player.getInventory().addItem(planBukkitStack);

        val hammerStack = new ItemStack(Items.STONE_AXE);
        val hammerBukkitStack = CraftItemStack.asCraftMirror(hammerStack);
        val hammerMeta = hammerBukkitStack.getItemMeta();
        hammerMeta.setDisplayName(ChatColor.RED + "Молот");
        hammerBukkitStack.setItemMeta(hammerMeta);
        hammerStack.getTag().setBoolean(PluginUtils.BREAK_ITEM_TAG, true);
        player.getInventory().addItem(hammerBukkitStack);

        player.sendMessage(ChatColor.GREEN + "Вы успешно получили предметы");
        return true;
    }
}
