package me.stepbystep.building.pattern.wall;

import me.stepbystep.building.BlockType;
import me.stepbystep.building.obj.BlockLocation;
import org.bukkit.util.Vector;

import java.util.LinkedHashMap;
import java.util.Map;

public class WindowPatternType extends AbstractWallPatternType {
    private final Map<Vector, BlockType> points = createPoints();

    public WindowPatternType() {
        super("WINDOW", "Оконный проем");
    }

    private Map<Vector, BlockType> createPoints() {
        Map<Vector, BlockType> result = new LinkedHashMap<>();
        for (Map.Entry<Vector, BlockType> entry : getRawPoints().entrySet()) {
            Vector vec = entry.getKey();
            BlockType blockType;
            if (vec.getBlockX() >= -1 && vec.getBlockX() <= 1 && vec.getBlockY() == 1)
                blockType = BlockType.AIR;
            else
                blockType = entry.getValue();

            result.put(vec, blockType);
        }

        return result;
    }

    @Override
    protected Map<Vector, BlockType> getPoints(BlockLocation center) {
        return points;
    }
}
