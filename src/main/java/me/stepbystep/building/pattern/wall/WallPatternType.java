package me.stepbystep.building.pattern.wall;

import me.stepbystep.building.BlockType;
import me.stepbystep.building.obj.BlockLocation;
import org.bukkit.util.Vector;

import java.util.Map;

public class WallPatternType extends AbstractWallPatternType {
    private final Map<Vector, BlockType> allPossiblePoints = getRawPoints();

    public WallPatternType() {
        super("WALL", "Стена");
    }

    @Override
    protected Map<Vector, BlockType> getPoints(BlockLocation center) {
        return allPossiblePoints;
    }
}
