package me.stepbystep.building.pattern.wall;

import lombok.val;
import me.stepbystep.building.BlockType;
import me.stepbystep.building.RotationQuarter;
import me.stepbystep.building.RustBuilding;
import me.stepbystep.building.obj.BlockLocation;
import me.stepbystep.building.obj.PatternBlock;
import me.stepbystep.building.pattern.PatternType;
import me.stepbystep.building.util.PluginUtils;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class AbstractWallPatternType extends PatternType {
    public AbstractWallPatternType(String name, String displayName) {
        super(name, displayName);
    }

    protected Map<Vector, BlockType> getRawPoints() {
        return
                PluginUtils.associate(
                        PluginUtils.group(0, WALL_HEIGHT - 1, -2, 2).stream()
                                .map(pair -> new Vector(pair.getY(), pair.getX(), 0)) // do not swap!
                                .collect(Collectors.toList()),
                        vec -> {
                            if (Math.abs(vec.getBlockX()) == 2 || Math.abs(vec.getBlockY()) == WALL_HEIGHT - 1)
                                return BlockType.BORDER;
                            else
                                return BlockType.INNER;
                        }
                );
    }

    @Override
    public boolean canBePlaced(
            BlockLocation center,
            Collection<BlockLocation> locations,
            RotationQuarter quarter,
            Map<BlockLocation, BlockType> blocks
    ) {
        return super.canBePlaced(center, locations, quarter, blocks) && onEdgeOfBasementOrWall(center, locations);
    }

    private boolean onEdgeOfBasementOrWall(BlockLocation center, Collection<BlockLocation> locations) {
        if (locations.isEmpty()) return true;

        val blockStorage = RustBuilding.INSTANCE.getBlockStorage();
        val crossingElements = new ArrayList<Collection<PatternBlock>>();

        for (val location : locations) {
            if (location.getY() != center.getY()) continue;
            val bottomLoc = location.getRelative(0, -1, 0);
            val allBottomBlocks = blockStorage.getAllPatternBlocks(bottomLoc);
            if (allBottomBlocks.isEmpty()) {
                return false;
            }
            val bottomPatternBlock = allBottomBlocks.minBy(PatternBlock::getBlockIndex);

            val patternType = bottomPatternBlock.getPatternType();
            // TODO: do we need this?
            if (patternType == BASEMENT && bottomPatternBlock.getBlockIndex() >= BASEMENT_LAYER_SIZE) {
                return false;
            }

            crossingElements.add(allBottomBlocks);
            if (patternType.isKindOf(WALL)) continue;
            if (patternType == STAIRS) {
                if (bottomPatternBlock.getBlockIndex() >= STAIRS_COUNT) continue;
                else {
                    return false;
                }
            }

            if (patternType != ROOF && patternType != BASEMENT) return false;
            if (!is5x5Edge(bottomPatternBlock.getBlockIndex())) return false;
        }
        for (val elements1 : crossingElements) {
            for (val elements2 : crossingElements) {
                if (elements1 != elements2 && PluginUtils.noElementIntersection(elements1, elements2)) {
                    return false;
                }
            }
        }

        return true;
    }

    @Override
    protected boolean shouldTruncateBlock(int newIndex, PatternBlock oldBlock, BlockLocation center) {
        val oldType = oldBlock.getPatternType();
        val oldIndex = oldBlock.getBlockIndex();
        if (oldType.isKindOf(WALL) && isEdge(oldIndex, 5, WALL_HEIGHT) && isEdge(newIndex, 5, WALL_HEIGHT)) {
//            return to5x5Coordinates(oldIndex).getY() == to5x5Coordinates(newIndex).getY();
            val oldY = to5x5Coordinates(oldIndex).getY();
            val newY = to5x5Coordinates(newIndex).getY();
            if (oldY == newY) return true;
            if (!(is5x5SideEdge(oldIndex) && is5x5SideEdge(newIndex))) return false;
            return Math.abs(oldY - newY) <= 1 || (oldY == 0 && newY == WALL_HEIGHT - 1) || (newY == 0 && oldY == WALL_HEIGHT - 1); // TODO: may introduce new bugs
        }
//        if (oldType == ROOF && is5x5Edge(oldIndex) && isWallTop(newIndex)) return true; // TODO: may introduce new bugs
        if (oldType == ROOF && is5x5Edge(oldIndex) && isWallTopSideOrBottomEdge(newIndex)) return true;
        if (oldType == BASEMENT && oldIndex < BASEMENT_LAYER_SIZE) {
            val layerCoords = PatternType.BASEMENT.getAbsCoordsOfLayer(oldIndex);
            return layerCoords.getX() == 2 && layerCoords.getY() == 2;
        }

        return oldType == STAIRS && oldIndex >= STAIRS_COUNT && isWallTopSideOrBottomEdge(newIndex);
    }

    private boolean isWallTopSideOrBottomEdge(int blockIndex) {
        return isWallTop(blockIndex) || isWallTop(blockIndex + WALL_WIDTH) || isWallBottomEdge(blockIndex);
    }
}
