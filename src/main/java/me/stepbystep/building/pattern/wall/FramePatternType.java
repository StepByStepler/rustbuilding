package me.stepbystep.building.pattern.wall;

import lombok.val;
import me.stepbystep.building.BlockType;
import me.stepbystep.building.obj.BlockLocation;
import org.bukkit.util.Vector;

import java.util.LinkedHashMap;
import java.util.Map;

public class FramePatternType extends AbstractWallPatternType {
    private final Map<Vector, BlockType> points = createPoints();

    public FramePatternType() {
        super("FRAME", "Рама");
    }

    private Map<Vector, BlockType> createPoints() {
        val result = new LinkedHashMap<Vector, BlockType>();
        for (val entry : getRawPoints().entrySet()) {
            val vec = entry.getKey();
            BlockType blockType;
            if (Math.abs(vec.getBlockX()) <= 1 && vec.getBlockY() <= 2)
                blockType = BlockType.AIR;
            else if (Math.abs(vec.getBlockX()) <= 1 && vec.getBlockY() == 3)
                blockType = BlockType.FRAME_TOP;
            else
                blockType = entry.getValue();

            result.put(vec, blockType);
        }

        return result;
    }

    @Override
    protected Map<Vector, BlockType> getPoints(BlockLocation center) {
        return points;
    }
}
