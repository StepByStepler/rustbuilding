package me.stepbystep.building.pattern;

import lombok.val;
import lombok.var;
import me.stepbystep.building.BlockType;
import me.stepbystep.building.RotationQuarter;
import me.stepbystep.building.RustBuilding;
import me.stepbystep.building.obj.BlockLocation;
import me.stepbystep.building.obj.PatternBlock;
import me.stepbystep.building.util.PluginUtils;
import org.bukkit.Material;
import org.bukkit.util.Vector;

import java.util.*;
import java.util.stream.Collectors;

public class StairsPatternType extends PatternType {
    public StairsPatternType() {
        super("STAIRS", "Лестница");
    }

    private final List<Vector> topBorderPoints = PluginUtils.group(-2, 2, -2, 2).stream()
            .filter(pair -> Math.abs(pair.getX()) == 2 || Math.abs(pair.getY()) == 2)
            .map(pair -> new Vector(pair.getX() + 1, 3, pair.getY() + 1))
            .collect(Collectors.toList());

    private final Map<Vector, BlockType> points = PluginUtils.concatMaps(
            PluginUtils.associate(Arrays.asList(
                    new Vector(0, 0, 0),
                    new Vector(0, 1, 1),
                    new Vector(0, 2, 2),
                    new Vector(1, 2, 2),
                    new Vector(2, 2, 2),
                    new Vector(2, 3, 1),
                    new Vector(2, 3, 0)
            ), vec -> BlockType.INNER),
            PluginUtils.associate(topBorderPoints, vec -> BlockType.BORDER)
    );

    @Override
    protected Map<Vector, BlockType> getPoints(BlockLocation center) {
        return points;
    }

    @Override
    protected boolean shouldTruncateBlock(int newIndex, PatternBlock oldBlock, BlockLocation center) {
        return newIndex >= STAIRS_COUNT && shouldTruncateRoofKind(oldBlock);
    }

    @Override
    public boolean canBePlaced(
            BlockLocation center,
            Collection<BlockLocation> locations,
            RotationQuarter quarter,
            Map<BlockLocation, BlockType> blocks
    ) {
        val stairs = new ArrayList<BlockLocation>(STAIRS_COUNT);
        val logs = new ArrayList<BlockLocation>(locations.size() - STAIRS_COUNT);
        val locsIterator = locations.iterator();
        for (var i = 0; i < STAIRS_COUNT && locsIterator.hasNext(); i++) {
            stairs.add(locsIterator.next());
        }
        while (locsIterator.hasNext()) {
            logs.add(locsIterator.next());
        }
        if (!super.canBePlaced(center, stairs, quarter, blocks)) return false;
        if (replaceWrongBlocks(logs)) return false;

        val bottomLoc = center.getRelative(0, -1, 0);
        val blockStorage = RustBuilding.INSTANCE.getBlockStorage();
        val bottomBlock = blockStorage.getAnyPatternBlock(bottomLoc);
        if (bottomBlock == null) return false;
        val bottomType = bottomBlock.getPatternType();
        if (bottomType == STAIRS && bottomBlock.getBlockIndex() == STAIRS_COUNT - 1) return isOnAnotherStair(locations);
        if (bottomType != BASEMENT && bottomType != ROOF) return false;

        val baseCoords = to5x5Coordinates(bottomBlock.getBlockIndex());
        val baseZ = baseCoords.getX() - 2; // dont ask why
        val baseX = baseCoords.getY() - 2;

        if (Math.abs(baseX) != 1 || Math.abs(baseZ) != 1) return false;
        if (baseX == -1 && PluginUtils.maxOf(stairs, BlockLocation::getX) - center.getX() != 2) return false;
        if (baseX == 1 && center.getX() - PluginUtils.minOf(stairs, BlockLocation::getX) != 2) return false;
        if (baseZ == -1 && PluginUtils.maxOf(stairs, BlockLocation::getZ) - center.getZ() != 2) return false;
        if (baseZ == 1 && center.getZ() - PluginUtils.minOf(stairs, BlockLocation::getZ) != 2) return false;

        if (hasBlockAboveFirst(locations)) return false;

        return true;
    }

    public Vector getVectorByIndex(int index) {
        val allPossiblePoints = points.keySet().iterator();
        for (int i = 0; i < index; i++) {
            allPossiblePoints.next();
        }
        return allPossiblePoints.next();
    }

    private boolean replaceWrongBlocks(List<BlockLocation> logs) {
        val blockStorage = RustBuilding.INSTANCE.getBlockStorage();
        for (val loc : logs) {
            if (loc.getBlock().getType() == Material.AIR) continue;
            val patternBlock = blockStorage.getAnyPatternBlock(loc);
            if (patternBlock == null || !patternBlock.getPatternType().isKindOf(WALL)) {
                return true;
            }
        }

        return false;
    }

    private boolean isOnAnotherStair(Collection<BlockLocation> locations) {
        val blockStorage = RustBuilding.INSTANCE.getBlockStorage();
        val iterator = locations.iterator();
        for (var i = 0; i < STAIRS_COUNT; i++) {
            iterator.next(); // skip stairs
        }

        while (iterator.hasNext()) { // maybe don't check all?
            val bottomLoc = iterator.next().getRelative(0, -WALL_HEIGHT, 0);
            if (blockStorage.getPatternBlockOfType(bottomLoc, PatternType.STAIRS) == null) {
                return false;
            }
        }

        return true;
    }

    private boolean hasBlockAboveFirst(Collection<BlockLocation> locations) {
        val firstLoc = locations.iterator().next();
        val aboveLoc = firstLoc.getRelative(0, 2, 0);
        return RustBuilding.INSTANCE.getBlockStorage().hasPatternBlock(aboveLoc);
    }
}
