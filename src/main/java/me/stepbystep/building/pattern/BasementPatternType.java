package me.stepbystep.building.pattern;

import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import lombok.val;
import lombok.var;
import me.stepbystep.building.BlockType;
import me.stepbystep.building.RotationQuarter;
import me.stepbystep.building.RustBuilding;
import me.stepbystep.building.db.BlockStorage;
import me.stepbystep.building.obj.BlockLocation;
import me.stepbystep.building.obj.PatternBlock;
import me.stepbystep.building.pattern.wall.AbstractWallPatternType;
import me.stepbystep.building.util.*;
import org.bukkit.util.Vector;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class BasementPatternType extends PatternType {
    public BasementPatternType() {
        super("BASEMENT", "Фундамент");
    }

    @Override
    protected boolean needsRotation() {
        return false;
    }

    private final IntOpenHashSet truncatedEdges = new IntOpenHashSet(
            IntStream.concat(
                    IntStream.range(0, 25).filter(PatternType::is5x5Edge),
                    IntStream.range(0, 7).map(index -> index + 25)
            ).boxed().collect(Collectors.toList())
    );

    private final Map<Vector, BlockType> basement = PluginUtils.associate(
            PluginUtils.group(-2, 2, -2, 2).stream()
                    .map(pair -> new Vector(pair.getX(), 0, pair.getY()))
                    .collect(Collectors.toList()),
            vec -> {
                if (Math.abs(vec.getBlockX()) == 2 || Math.abs(vec.getBlockZ()) == 2)
                    return BlockType.BORDER;
                else
                    return BlockType.INNER;
            }
    );

    private final IntPair[] layerAbsCoordinates = IntStream.range(0, BASEMENT_LAYER_SIZE)
            .mapToObj(index -> {
                val coordinates = to5x5Coordinates(index);
                val x = Math.abs(coordinates.getX() - 2);
                val y = Math.abs(coordinates.getY() - 2);
                return new IntPair(x, y);
            })
            .toArray(IntPair[]::new);

    private final IntPair[][] neighbourEdges = {
            neighbourEdge(x -> new IntPair(x, -3)),
            neighbourEdge(x -> new IntPair(x, 3)),
            neighbourEdge(z -> new IntPair(-3, z)),
            neighbourEdge(z -> new IntPair(3, z)),
    };

    private final List<IntPair> columnDiffs = PluginUtils.group(-2, 2, -2, 2)
            .stream()//.filter(pair -> Math.abs(pair.getX()) == 2 || Math.abs(pair.getY()) == 2)
            .collect(Collectors.toList());

    private final IntPair[] edgePoints = PluginUtils.group(-2, 2, -2, 2)
            .stream().filter(pair -> Math.abs(pair.getX()) == 2 || Math.abs(pair.getY()) == 2)
            .collect(Collectors.toList())
            .toArray(new IntPair[0]);

    @Override
    protected Map<Vector, BlockType> getPoints(BlockLocation center) {
        Map<Vector, BlockType> result = new LinkedHashMap<>(basement);
        for (int x = -2; x <= 2; x++) {
            for (int z = -2; z <= 2; z++) {
                addColumns(result, center, x, z);
            }
        }
//            for (IntPair entry : columnDiffs) {
//                addColumns(result, center, entry.getX(), entry.getY());
//            }

        return result;
    }

    private IntPair borderIndexTo5x5Coordinates(int index) {
        if (index >= 0 && index <= 5)
            return to5x5Coordinates(index);
        else if (index == 6 || index == 7)
            return to5x5Coordinates(index + 3);
        else if (index == 8 || index == 9)
            return to5x5Coordinates(index + 6);
        else
            return to5x5Coordinates(index + 9);
    }

    public IntPair getAbsCoordsOfLayer(int blockIndex) {
        return layerAbsCoordinates[blockIndex];
    }

    private IntPair[] neighbourEdge(Function<Integer, IntPair> getPoint) {
        IntPair[] result = new IntPair[7];
        for (int i = 0; i < result.length; i++) {
            result[i] = getPoint.apply(i - 3);
        }
        return result;
    }

    private void addColumns(Map<Vector, BlockType> blocks, BlockLocation center, int dx, int dz) {
        val blockType = getBlockType(dx, dz);
        val blockStorage = RustBuilding.INSTANCE.getBlockStorage();
        val isFirstEmpty = isNoBlockAt(center, dx, -1, dz, blockStorage);
        val firstBlockType = isFirstEmpty ? blockType : null;
        BlockType secondBlockType;
        if (isFirstEmpty && isNoBlockAt(center, dx, -2, dz, blockStorage))
            secondBlockType = blockType;
        else
            secondBlockType = null;

        blocks.put(new Vector(dx, -1, dz), firstBlockType);
        blocks.put(new Vector(dx, -2, dz), secondBlockType);
    }

    private boolean isNoBlockAt(BlockLocation center, int dx, int dy, int dz, BlockStorage blockStorage) {
        if (center.getBlock(dx, dy, dz).getType().isSolid()) return false;
        val relativeLoc = center.getRelative(dx, dy, dz);
        return !blockStorage.hasPatternBlock(relativeLoc);
    }

    private BlockType getBlockType(int dx, int dz) {
        if (Math.abs(dx) == 2 && Math.abs(dz) == 2)
            return BlockType.BORDER;
        else
            return BlockType.FAKE_BASEMENT_INNER;
    }

    @Override
    public boolean canBePlaced(
            BlockLocation center,
            Collection<BlockLocation> locations,
            RotationQuarter quarter,
            Map<BlockLocation, BlockType> blocks
    ) {
        if (!super.canBePlaced(center, locations, quarter, blocks)) return false;
        if (hasNoTruncatedBlocksV2(center) && hasBuildingsAround(center)) return false;
        if (hasContactedNeighbours(center)) return false;

        val minColumnY = new HashMap<IntPair, Integer>();
        for (val loc : locations) {
            val dx = loc.getX() - center.getX();
            val dz = loc.getZ() - center.getZ();
            val pair = new IntPair(dx, dz);
            val minY = minColumnY.getOrDefault(pair, Integer.MAX_VALUE);
            if (loc.getY() < minY) {
                minColumnY.put(pair, loc.getY());
            }
        }

        val blockStorage = RustBuilding.INSTANCE.getBlockStorage();
        val centerY = center.getY();
        for (val entry : minColumnY.entrySet()) {
            val coords = entry.getKey();
            val minY = entry.getValue();
            if (minY == Integer.MAX_VALUE) continue;
            val blockBelow = center.getBlock(coords.getX(), minY - center.getY() - 1, coords.getY());
            if (!blockBelow.getType().isSolid()) return false;
            val pBlockBelow = blockStorage.getAnyPatternBlock(blockBelow);
            if (isBlockBelowWrong(coords, centerY - (minY - 1), pBlockBelow)) return false;
        }

        return true;
    }

    private boolean isBlockBelowWrong(IntPair newCoords, int yDiff, PatternBlock patternBlock) {
        if (patternBlock == null) return false;
        if (yDiff > 1) return true;
        if (patternBlock.getPatternType() != BASEMENT) return true;
        val oldAbsCoords = layerAbsCoordinates[patternBlock.getBlockIndex() % BASEMENT_LAYER_SIZE];
        if (oldAbsCoords.getX() != 2 && oldAbsCoords.getY() != 2) return true;

        val oldDist = sqr(oldAbsCoords.getX()) + sqr(oldAbsCoords.getY());
        val newDist = sqr(newCoords.getX()) + sqr(newCoords.getY());
        return oldDist != newDist;
    }

    @Override
    protected boolean shouldTruncateBlock(int newIndex, PatternBlock oldBlock, BlockLocation center) {
        val oldType = oldBlock.getPatternType();
        if (oldType instanceof AbstractWallPatternType) {
            if (newIndex >= BASEMENT_LAYER_SIZE) return false;
            val layerCoords = layerAbsCoordinates[newIndex];
            return layerCoords.getX() == 2 && layerCoords.getY() == 2 && isWallBottomEdge(oldBlock.getBlockIndex());
        }
        if (oldType != BASEMENT) return false;
        if (!truncatedEdges.contains(newIndex)) return false;

        int oldIndex = oldBlock.getBlockIndex();
        if (newIndex >= BASEMENT_LAYER_SIZE && oldIndex >= BASEMENT_LAYER_SIZE)
            return true; // it's hard to compare bottom columns

//            IntTriple oldCoords = blockCoordinates[oldIndex];
//            IntTriple newCoords = blockCoordinates[newIndex];
//            if (newCoords.getFirst() != oldCoords.getFirst()) return false;
        val oldCoords = getBlockCoordinates(oldIndex);
        val newCoords = getBlockCoordinates(newIndex);


//            if (newCoords.getX() == oldCoords.getX() && newCoords.getY() == oldCoords.getY())
//                return true;
//            if (newCoords.getX() == oldCoords.getY() && newCoords.getY() == oldCoords.getX())
//                return true;
        if (Math.abs(newCoords.getFirst() - oldCoords.getFirst()) > 1) return false;
        if (newCoords.getSecond() == oldCoords.getSecond() && newCoords.getThird() == oldCoords.getThird())
            return true;
        if (newCoords.getSecond() == oldCoords.getThird() && newCoords.getThird() == oldCoords.getSecond())
            return true;

        return false;
    }

    public int getPointY(int blockIndex) {
        if (blockIndex < BASEMENT_LAYER_SIZE) return 0;
        val columnIndex = blockIndex - BASEMENT_LAYER_SIZE;
        return columnIndex % 2 == 0 ? -1 : -2;
    }

    public IntTriple getBlockCoordinates(int blockIndex) {
        if (blockIndex < BASEMENT_LAYER_SIZE) {
            val layerCoords = layerAbsCoordinates[blockIndex];
            return new IntTriple(0, layerCoords.getX(), layerCoords.getY());
        }
        val columnIndex = blockIndex - BASEMENT_LAYER_SIZE;
        val coords = to5x5Coordinates(columnIndex / 2);
        val y = getPointY(blockIndex);
        return new IntTriple(y, Math.abs(coords.getX() - 2), Math.abs(coords.getY() - 2));
    }

    private boolean hasNoTruncatedBlocks(Collection<BlockLocation> blocks) {
        int maxY = Integer.MIN_VALUE;
        int maxCount = 0;
        for (BlockLocation loc : blocks) {
            if (loc.getY() > maxY) {
                maxY = loc.getY();
                maxCount = 1;
            } else if (loc.getY() == maxY) {
                maxCount++;
            }
        }

        return maxCount == BASEMENT_LAYER_SIZE;
    }

    private boolean hasNoTruncatedBlocksV2(BlockLocation center) {
        val blockStorage = RustBuilding.INSTANCE.getBlockStorage();
        for (val point : edgePoints) {
            for (var y = -1; y <= 1; y++) {
                val nearLoc = center.getRelative(point.getX(), y, point.getY());
                val block = blockStorage.getAnyPatternBlock(nearLoc);
                if (block != null && block.getPatternType() == BASEMENT) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean hasBuildingsAround(BlockLocation location) {
        val blockStorage = RustBuilding.INSTANCE.getBlockStorage();
        for (var dy = 0; dy >= -BASEMENT_HEIGHT; dy--) {
            for (var dx = -BASEMENT_BORDER_RADIUS; dx <= BASEMENT_BORDER_RADIUS; dx++) {
                for (var dz = -BASEMENT_BORDER_RADIUS; dz <= BASEMENT_BORDER_RADIUS; dz++) {
                    val relativeLoc = location.getRelative(dx, dy, dz);
                    if (blockStorage.hasPatternBlock(relativeLoc)) return true;
                }
            }
        }

        return false;
    }

    private boolean hasContactedNeighbours(BlockLocation center) {
        val blockStorage = RustBuilding.INSTANCE.getBlockStorage();
        for (val edge : neighbourEdges) {
            var hasBorderBlock = false;
            edgeLoop:
            for (val vec : edge) {
                for (var dy = -10; dy <= 10; dy++) { // TODO: change to 0..255 using cached blocks on x-z ignoring height
                    val relativeLoc = center.getRelative(vec.getX(), dy, vec.getY());
                    val block = blockStorage.getAnyPatternBlock(relativeLoc);
                    if (block == null) continue;
                    val blockType = block.getBlockType();
                    if (blockType == BlockType.INNER) {
                        hasBorderBlock = false;
                        break edgeLoop;
                    } else if (blockType == BlockType.BORDER) {
                        hasBorderBlock = true;
                    }
                }
            }
            if (hasBorderBlock) {
                return true;
            }
        }

        return false;
    }
}
