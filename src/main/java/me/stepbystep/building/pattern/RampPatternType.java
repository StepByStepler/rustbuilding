package me.stepbystep.building.pattern;

import com.google.common.collect.Iterables;
import lombok.val;
import me.stepbystep.building.BlockType;
import me.stepbystep.building.RotationQuarter;
import me.stepbystep.building.RustBuilding;
import me.stepbystep.building.obj.BlockLocation;
import me.stepbystep.building.obj.PatternBlock;
import me.stepbystep.building.util.IntPair;
import me.stepbystep.building.util.IntTriple;
import org.bukkit.util.Vector;
import org.eclipse.collections.impl.list.mutable.FastList;

import java.util.*;

public class RampPatternType extends PatternType {
    private final Map<Vector, BlockType> points = createPoints();
    private final List<IntTriple> orderedPoints = new FastList<>(points.keySet()).collect(vec ->
        new IntTriple(vec.getBlockX(), vec.getBlockY(), vec.getBlockZ())
    );

    public RampPatternType() {
        super("RAMP", "Рампа");
    }

    private Map<Vector, BlockType> createPoints() {
        val result = new LinkedHashMap<Vector, BlockType>();
        val yzDiffs = Arrays.asList(new IntPair(0, -1), new IntPair(0, 0), new IntPair(1, 0));

        for (val yzDiff : yzDiffs) {
            for (int x = -1; x <= 1; x++) {
                result.put(new Vector(x, yzDiff.getX(), yzDiff.getY()), BlockType.INNER);
            }
        }

        return result;
    }

    @Override
    protected Map<Vector, BlockType> getPoints(BlockLocation center) {
        return points;
    }

    @Override
    public boolean canBePlaced(
            BlockLocation center,
            Collection<BlockLocation> locations,
            RotationQuarter quarter,
            Map<BlockLocation, BlockType> blocks
    ) {
        if (!super.canBePlaced(center, locations, quarter, blocks)) return false;
        if (!isOnGround(center, locations)) return false;
        if (!isNearBasement(center, locations, quarter)) return false;
        if (isHighestBlockNearBasementBottom(locations, quarter)) return false;

        return true;
    }

    private boolean isOnGround(BlockLocation center, Collection<BlockLocation> locations) {
        val blockStorage = RustBuilding.INSTANCE.getBlockStorage();

        for (val location : locations) {
            if (location.getY() > center.getY()) continue;
            val locBelow = location.getRelative(0, -1, 0);
            val patternBlockBelow = blockStorage.getPatternBlockOfDifferentType(locBelow, PatternType.RAMP);
            if (patternBlockBelow != null) {
                return false;
            }
            if (!locBelow.getBlock().getType().isSolid()) {
                return false;
            }
        }

        return true;
    }

    private boolean isNearBasement(BlockLocation center, Collection<BlockLocation> locations, RotationQuarter quarter) {
        val direction = quarter.rotate2D(new Vector(0, 0, 1));
        val blockStorage = RustBuilding.INSTANCE.getBlockStorage();

        for (val location : locations) {
            val nearLoc = location.getRelative(direction.getX(), 0, direction.getY());
            val patternBlock = blockStorage.getPatternBlockWithType(nearLoc, PatternType.BASEMENT);
            if (patternBlock != null) {
                val rampX = (int) quarter.opposite().rotateX(location.subtract(center));
                if (isSameX(patternBlock, rampX)) continue;
            }
            if (!locations.contains(nearLoc)) {
                return false;
            }
        }

        return true;
    }

    private boolean isHighestBlockNearBasementBottom(Collection<BlockLocation> locations, RotationQuarter quarter) {
        if (locations.isEmpty()) return true;

        val direction = quarter.rotate2D(new Vector(0, 0, 1));
        val highestBlock = Iterables.getLast(locations);
        val relativeBlock = highestBlock.getRelative(direction.getX(), 0, direction.getY());
        val patternBlock = RustBuilding.INSTANCE.getBlockStorage().getAnyPatternBlock(relativeBlock);
        if (patternBlock == null || patternBlock.getPatternType() != BASEMENT) return false;
        return PatternType.BASEMENT.getPointY(patternBlock.getBlockIndex()) <= -2;
    }

    @Override
    protected boolean shouldTruncateBlock(int newIndex, PatternBlock oldBlock, BlockLocation center) {
        if (oldBlock.getPatternType() == BASEMENT) {
            val rampX = orderedPoints.get(newIndex).getFirst();
            return isSameX(oldBlock, rampX);
        } else if (oldBlock.getPatternType() == RAMP) {
            val oldPoint = orderedPoints.get(oldBlock.getBlockIndex());
            val newPoint = orderedPoints.get(newIndex);
            return oldPoint.getSecond() == newPoint.getSecond();
        }

        return false;
    }

    private boolean isSameX(PatternBlock basementBlock, int rampX) {
        val basementCoords = PatternType.BASEMENT.getBlockCoordinates(basementBlock.getBlockIndex());
        val basementX = basementCoords.getSecond();
        val basementZ = basementCoords.getThird();
        val absRampX = Math.abs(rampX);
        return (basementX == 2 && basementZ == absRampX) || (basementZ == 2 && basementX == absRampX);
    }
}
