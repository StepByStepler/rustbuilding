package me.stepbystep.building.pattern;

import lombok.val;
import lombok.var;
import me.stepbystep.building.BlockType;
import me.stepbystep.building.RotationQuarter;
import me.stepbystep.building.RustBuilding;
import me.stepbystep.building.obj.BlockLocation;
import me.stepbystep.building.obj.PatternBlock;
import me.stepbystep.building.pattern.wall.AbstractWallPatternType;
import me.stepbystep.building.util.IntPair;
import me.stepbystep.building.util.PluginUtils;
import org.bukkit.util.Vector;

import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

public class RoofPatternType extends PatternType {
    private static final int RADIUS = 2;
    public RoofPatternType() {
        super("ROOF", "Потолок");
    }

    @Override
    protected boolean needsRotation() {
        return false;
    }

    private final Map<Vector, BlockType> points = PluginUtils.associate(
            PluginUtils.group(-RADIUS, RADIUS, -RADIUS, RADIUS)
                    .stream().map(pair -> new Vector(pair.getX(), 0, pair.getY()))
                    .collect(Collectors.toList()),
            vec -> {
                if (Math.abs(vec.getBlockX()) == 2 || Math.abs(vec.getBlockZ()) == 2)
                    return BlockType.BORDER;
                else
                    return BlockType.INNER;
            }
    );

    private final IntPair[] edgePoints = PluginUtils.group(-RADIUS, RADIUS, -RADIUS, RADIUS)
            .stream().filter(pair -> (Math.abs(pair.getX()) == 2) != (Math.abs(pair.getY()) == 2))
            .toArray(IntPair[]::new);

    private final IntPair[] insidePoints = PluginUtils.group(-RADIUS, RADIUS, -RADIUS, RADIUS)
            .stream().filter(pair -> (Math.abs(pair.getX()) != 2) && (Math.abs(pair.getY()) != 2))
            .toArray(IntPair[]::new);

    @Override
    protected Map<Vector, BlockType> getPoints(BlockLocation center) {
        return points;
    }

    @Override
    public boolean canBePlaced(
            BlockLocation center,
            Collection<BlockLocation> locations,
            RotationQuarter quarter,
            Map<BlockLocation, BlockType> blocks
    ) {
//        return super.canBePlaced(center, locations) && hasEnoughPlacedEdges(center) && //!hasBlockUnderOrOver(locations)
        if (!super.canBePlaced(center, locations, quarter, blocks)) return false;
        if (hasBlocksBelow(center, -WALL_HEIGHT + 1)) return false;
        if (hasBlocksBelow(center, -1) || hasBlocksBelow(center, 1)) return false;
//        if (hasBlocksBelow(center, -WALL_HEIGHT, true)) return false;

        return hasEnoughPlacedEdges(center);
    }

    @Override
    protected boolean shouldTruncateBlock(int newIndex, PatternBlock oldBlock, BlockLocation center) {
        // OLD IMPL:
        // return is5x5Edge(newIndex) && shouldTruncateRoofKind(oldBlock);

//        if (hasBlocksBelow(center, -WALL_HEIGHT + 1, false)) return false;

        if (is5x5Edge(newIndex) && shouldTruncateRoofKind(oldBlock)) return true;
        // TODO: can break something
        if (!(oldBlock.getPatternType() instanceof AbstractWallPatternType)) return false;
        if (isWallTop(oldBlock.getBlockIndex() + WALL_WIDTH)) return true;
//        if (hasBlocksBelow(center, -WALL_HEIGHT, true)) return false;

        return is5x5Corner(newIndex) && isWallBottomEdge(oldBlock.getBlockIndex());
    }

    private boolean hasBlocksBelow(BlockLocation center, int yDiff) {
        val blockStorage = RustBuilding.INSTANCE.getBlockStorage();
        for (val point : insidePoints) {
            val bottomLoc = center.getRelative(point.getX(), yDiff, point.getY());
            val hasBottomBlock = blockStorage.hasPatternBlock(bottomLoc);
            if (hasBottomBlock) return true;
        }

        return false;
    }

    private boolean hasEnoughPlacedEdges(BlockLocation center) {
        val blockStorage = RustBuilding.INSTANCE.getBlockStorage();
        var hasStrongSide = false;
        var placedEdgePoints = 0;
        for (val pair : edgePoints) {
            val loc = center.getRelative(pair.getX(), 0, pair.getY());
            val patternBlocks = blockStorage.getAllPatternBlocks(loc);
            val patternBlock = findPriorityPatternBlock(patternBlocks);
            if (patternBlock == null) continue;

            val patternType = patternBlock.getPatternType();
            if (patternType.isKindOf(WALL)) {
                val wallX = patternBlock.getBlockIndex() % WALL_WIDTH;
                if (wallX == 0 || wallX == WALL_WIDTH - 1)
                    return false; // instantly stop if we have at least 1 wrong edge
                if (isWallTop(patternBlock.getBlockIndex())) {
                    placedEdgePoints += 2;
                    hasStrongSide = true;
                }
            } else if (patternType == ROOF) {
                if (is5x5Edge(patternBlock.getBlockIndex())) {
                    placedEdgePoints++;
                } else {
                    return false;
                }
            } else if (patternType == STAIRS) {
                val blockIndex = patternBlock.getBlockIndex();
                val vec = STAIRS.getVectorByIndex(blockIndex);
                if (blockIndex >= STAIRS_COUNT && (Math.abs(vec.getBlockX() - 1) != 2 || Math.abs(vec.getBlockZ() - 1) != 2)) { // it is roof and not on corner
                    placedEdgePoints += 2;
                    hasStrongSide = true;
                } else {
                    return false;
                }
            }
        }

        return hasStrongSide && placedEdgePoints >= 6; // cannot early-return because of additional checks
    }

    private PatternBlock findPriorityPatternBlock(Iterable<PatternBlock> blocks) {
        PatternBlock lastRoofBlock = null;
        for (val block : blocks) {
            val type = block.getPatternType();
            if (type.isKindOf(WALL) || type == STAIRS) return block;
            else if (type == ROOF) lastRoofBlock = block;
        }

        return lastRoofBlock;
    }
}
