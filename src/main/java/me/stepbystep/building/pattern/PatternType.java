package me.stepbystep.building.pattern;

import lombok.Getter;
import lombok.Setter;
import lombok.val;
import lombok.var;
import me.stepbystep.building.BlockType;
import me.stepbystep.building.RotationQuarter;
import me.stepbystep.building.RustBuilding;
import me.stepbystep.building.obj.BlockLocation;
import me.stepbystep.building.obj.ElementPlacementScope;
import me.stepbystep.building.obj.PatternBlock;
import me.stepbystep.building.pattern.wall.*;
import me.stepbystep.building.util.IntPair;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import java.util.*;

@Getter
@SuppressWarnings("StaticInitializerReferencesSubClass")
public abstract class PatternType {
    private static final Map<String, PatternType> byName = new HashMap<>();
    public static final BasementPatternType BASEMENT = new BasementPatternType();
    public static final PatternType WALL = new WallPatternType();
    public static final PatternType DOOR = new DoorPatternType();
    public static final PatternType ROOF = new RoofPatternType();
    public static final StairsPatternType STAIRS = new StairsPatternType();
    public static final PatternType WINDOW = new WindowPatternType();
    public static final PatternType RAMP = new RampPatternType();
    public static final PatternType FRAME = new FramePatternType();

    protected static final int BASEMENT_LAYER_SIZE = 25;
    protected static final int BASEMENT_COLUMN_SIZE = 2;
    protected static final int WALL_HEIGHT = 4;
    protected static final int WALL_WIDTH = 5;
    protected static final int STAIRS_COUNT = 7;
    protected static final int BASEMENT_BORDER_RADIUS = 6;
    protected static final int BASEMENT_HEIGHT = 2;

    private final String name;
    private final String displayName;
    @Setter private ItemStack correctHologramDisplay = new ItemStack(Material.AIR);
    @Setter private ItemStack wrongHologramDisplay = new ItemStack(Material.AIR);

    public static PatternType valueOf(String name) {
        val result = byName.get(name);
        if (result == null) throw new IllegalArgumentException("No PatternType found with name = " + name);
        return result;
    }

    protected PatternType(String name, String displayName) {
        this.name = name;
        this.displayName = displayName;

        byName.put(name, this);
    }

    protected boolean needsRotation() {
        return true;
    }

    protected abstract Map<Vector, BlockType> getPoints(BlockLocation center);

    public boolean canBePlaced(
            BlockLocation center,
            Collection<BlockLocation> locations,
            RotationQuarter quarter,
            Map<BlockLocation, BlockType> blocks
    ) {
        val blockStorage = RustBuilding.INSTANCE.getBlockStorage();
        return !locations.isEmpty() && locations.stream().noneMatch(location -> {
            if (blockStorage.hasPatternBlock(location)) return true;
            if (!location.getBlock().getType().isSolid()) return false;
            return blocks.get(location) != BlockType.AIR;
        });
    }

    protected abstract boolean shouldTruncateBlock(int newIndex, PatternBlock oldBlock, BlockLocation center);

    public final ElementPlacementScope getLocations(BlockLocation center, RotationQuarter quarter) {
        val actualPoints = getActualPoints(quarter, center);
        val blocks = new LinkedHashMap<BlockLocation, BlockType>(actualPoints.size());
        for (val entry : actualPoints.entrySet()) {
            val vector = entry.getKey();
            val blockLocation = new BlockLocation(
                    center.getWorld(),
                    center.getX() + vector.getBlockX(),
                    center.getY() + vector.getBlockY(),
                    center.getZ() + vector.getBlockZ()
            );
            blocks.put(blockLocation, entry.getValue());
        }
        val locations = getTruncatedLocations(blocks, center);
        return new ElementPlacementScope(
                blocks, locations, center, quarter, this,
                canBePlaced(center, locations, quarter, blocks), null, true
        );
    }

    private List<BlockLocation> getTruncatedLocations(Map<BlockLocation, BlockType> blocks, BlockLocation center) {
        val blockStorage = RustBuilding.INSTANCE.getBlockStorage();
        var i = -1;
        val res = new ArrayList<BlockLocation>();

        for (val entry : blocks.entrySet()) {
            i++;
            if (entry.getValue() == null) continue;

            val placedBlocks = blockStorage.getAllPatternBlocks(entry.getKey());
            if (!shouldTruncateAnyOf(i, placedBlocks, center)) {
                res.add(entry.getKey());
            }
        }

        return res;
    }

    private boolean shouldTruncateAnyOf(int blockIndex, Iterable<PatternBlock> blocks, BlockLocation center) {
        for (val block : blocks) {
            if (shouldTruncateBlock(blockIndex, block, center)) {
                return true;
            }
        }
        return false;
    }

    private Map<Vector, BlockType> getActualPoints(RotationQuarter quarter, BlockLocation center) {
        if (!needsRotation()) return getPoints(center);

        val actualPoints = new LinkedHashMap<Vector, BlockType>();
        for (val entry : getPoints(center).entrySet()) {
            val oldVector = entry.getKey();
            val newVector = new Vector(quarter.rotateX(oldVector), oldVector.getY(), quarter.rotateZ(oldVector));
            actualPoints.put(newVector, entry.getValue());
        }

        return actualPoints;
    }

    protected static IntPair to5x5Coordinates(int index) {
        return toCoordinates(index, 5);
    }

    protected static IntPair toCoordinates(int index, int size) {
        return new IntPair(index % size, index / size);
    }

    protected static boolean is5x5Edge(int index) {
        return isEdge(index, 5, 5);
    }

    protected static boolean isEdge(int index, int size) {
        return isEdge(index, size, size);
    }

    protected static boolean isEdge(int index, int width, int height) {
        val first = index % width;
        if (first == 0 || first == (width - 1)) return true;

        val second = index / width;
        return second == 0 || second == (height - 1);
    }
    protected static boolean is5x5SideEdge(int index) {
        val x = index % 5;
        return x == 0 || x == 4;
    }
    protected static boolean is5x5Corner(int index) {
        val coords = to5x5Coordinates(index);
        val x = coords.getX();
        val y = coords.getY();
        return (x == 0 || x == 4) && (y == 0 || y == 4);
    }

    protected static boolean is5x5Edge(IntPair coords) {
        val x = coords.getX();
        if (x == 0 || x == 4) return true;
        val y = coords.getY();
        return y == 0 || y == 4;
    }

    protected static boolean isWallTop(int index) {
        return (index / WALL_WIDTH) == WALL_HEIGHT - 1;
    }
    protected static boolean isWallBottomEdge(int index) {
        return index == 0 || index == WALL_WIDTH - 1;
    }

    protected static int sqr(int num) {
        return num * num;
    }

    protected static boolean shouldTruncateRoofKind(PatternBlock block) {
        val oldType = block.getPatternType();
        val oldIndex = block.getBlockIndex();
        if (oldType.isKindOf(WALL)) {
            if (isWallTop(oldIndex)) return true;
            // TODO
        }
        if (oldType == ROOF && is5x5Edge(oldIndex)) return true;
        if (oldType == STAIRS && oldIndex >= STAIRS_COUNT) return true;

        return false;
    }

    public boolean isKindOf(PatternType other) {
        if (this == BASEMENT || this == STAIRS || this == ROOF) {
            return this == other;
        }
        return other instanceof AbstractWallPatternType;
    }
}
