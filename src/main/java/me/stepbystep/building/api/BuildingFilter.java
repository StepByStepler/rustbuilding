package me.stepbystep.building.api;

import me.stepbystep.building.obj.BuildingContext;
import me.stepbystep.building.obj.ElementPlacementScope;

@FunctionalInterface
public interface BuildingFilter {
    boolean test(BuildingContext ctx, ElementPlacementScope scope);
}
