package me.stepbystep.building.api;

import lombok.NonNull;
import me.stepbystep.building.obj.BuildingElement;
import me.stepbystep.building.obj.ElementPlacementScope;
import me.stepbystep.building.obj.PatternBlock;
import me.stepbystep.building.pattern.PatternType;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Optional;

public interface RustBuildingApi {
    Optional<BuildingElement> placeElement(@NonNull Player player, @NonNull PatternType patternType);

    boolean isDoorwayBlock(@NonNull Location location);

    void upgradeElement(@NonNull Player player, int newLevel);

    void upgradeElement(@NonNull BuildingElement element, int newLevel);

    void breakElement(@NonNull Player player, boolean fireEvents);

    /**
     * @return list of all broken elements except given element
     */
    List<BuildingElement> breakElement(@NonNull BuildingElement element, boolean fireEvents);

    Optional<PatternBlock> getSingleBlockAt(@NonNull Location location);

    ElementPlacementScope findElementPlace(@NonNull Player player, @NonNull PatternType patternType);

    void addFilter(@NonNull BuildingFilter filter);

    void addDynamicFilter(@NonNull BuildingFilter filter);

    Optional<BuildingElement> findBuilding(@NonNull Location loc);
}
