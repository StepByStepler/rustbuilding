package me.stepbystep.building.api.event;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import me.stepbystep.building.obj.BuildingElement;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

@Data
@EqualsAndHashCode(callSuper = false)
public class BuildingElementBreakEvent extends Event {
    @Getter
    private static final HandlerList handlerList = new HandlerList();

    private final BuildingElement element;

    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }
}
